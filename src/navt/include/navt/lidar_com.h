#ifndef LIDAR_COM_H
#define LIDAR_COM_H

#include <iostream>
#include <string>
#include <pcap.h>
#include <sstream>
#include <stdint.h>
#include "math.h"
#include "std_msgs/String.h"
#include <navt/lidar_msgs.h>

class lidar_com
{

public:

	navt::point point;
	navt::scan scan;
	navt::scan_pkt scan_pkt;

	lidar_com& init();
	lidar_com& getScanPkt();

private:

	pcap_t *handle;         /* Session handle */
	char *dev;          /* The device to sniff on */
	char errbuf[PCAP_ERRBUF_SIZE];  /* Error string */
	struct bpf_program fp;      /* The compiled filter */
	char filter_exp[];    /* The filter expression */
	bpf_u_int32 mask;       /* Our netmask */
	bpf_u_int32 net;        /* Our IP */
	struct pcap_pkthdr header;  /* The header that pcap gives us */
	const u_char *data;       /* The actual packet */

	int block_offset;
	int laser_offset;
	u_int packetCount;

	uint16_t current_azimuth;
	uint8_t arr_intensity[32];
	uint16_t arr_distance[32];
	int intensity_index;
	int distance_index;

};

#endif
