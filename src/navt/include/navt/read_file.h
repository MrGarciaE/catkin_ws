#ifndef READ_FILE_H
#define READ_FILE_H

#include <fstream>
#include <istream>
#include <string>
#include <navt/lidar_msgs.h>

class read_file
{

public:
  navt::scan_pkt scan_pkt;

  read_file& read(std::string filename);
  std::string fpath;

private:
  std::ifstream data;

  std::string vert_str, azm_str, dist_str;
  double vert_num, azm_num, dist_num;

  navt::scan scan;
  navt::point point;
};

#endif
