#ifndef RVIZ_H
#define RVIZ_H

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <navt/lidar_msgs.h>

class rviz
{
public:
  rviz& init();
  rviz& publishLines(navt::line_set &line_stk);
  rviz& publishPoints(navt::scan_pkt &scan_pkt);
  rviz& publishRegions(navt::region_pkt &closedRegions);
  rviz& publishZeroPoints(navt::scan_pkt &scan_pkt);
  rviz& publishObstaclePoints(navt::scan_pkt &scan_pkt);
  rviz& publishAll(navt::line_set &line_stk, navt::scan_pkt &scan_pkt);

private:
  ros::NodeHandle n;
  ros::Publisher vpoints, vlines, vregions, zeroPoints, obstaclePoints;
  visualization_msgs::Marker points, line_list, regions_linelist;
	geometry_msgs::Point p;
  std_msgs::ColorRGBA c;

};

#endif
