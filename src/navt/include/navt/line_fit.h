#ifndef LINE_FIT_H
#define LINE_FIT_H

#include <navt/lidar_msgs.h>
#include <vector>

class line_fit
{

public:
	navt::line_set line_set;
	navt::azm azm;
	navt::scan_pkt scan_pkt_xyz;
	navt::intensities intensitiesId;

	line_fit& getLineStock(navt::scan_pkt &scan_pkt);

private:
	line_fit& getScanLines(navt::scan scan);
	int numof_scan;
};

#endif
