#ifndef LAND_SEGMENTATION_H
#define LAND_SEGMENTATION_H

#include <navt/lidar_msgs.h>

class land_segmentation
{

public:
  land_segmentation& getRegions(navt::line_set line_set);
  navt::region_pkt closedRegions;

private:
  land_segmentation& init(navt::azm firstAzm);
  land_segmentation& updateRegionParameters(navt::region &region);
  land_segmentation& newRegion(navt::line newLine);
  land_segmentation& newObstacle(navt::line newLine);
  land_segmentation& growRegion(navt::region_pkt &possibleRegions,
    int r, navt::line newLine, int fusion);
  land_segmentation& closeRegion(int r);
  navt::region_pkt possibleRegions;

};

#endif
