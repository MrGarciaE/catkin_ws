#include <navt/lidar_com.h>
#include <algorithm>

const float vert_angle[32][2] =
{
	{	-0.5352924816,	-30.6280068297	},
	{	-0.1628392175,	-9.3172253231		},
	{	-0.5119050696,	-29.2898415493	},
	{	-0.1396263402,	-7.9890464505		},
	{	-0.4886921906,	-27.9616625769	},
	{	-0.1164134629,	-6.660867578		},
	{	-0.4654793115,	-26.6334836045	},
	{	-0.0930260474,	-5.3227020978		},
	{	-0.4420918995,	-25.295318324		},
	{	-0.0698131701,	-3.9945232253		},
	{	-0.4188790205,	-23.9671393516	},
	{	-0.0466002928,	-2.6663443527		},
	{	-0.3956661414,	-22.6389603792	},
	{	-0.0232128791,	-1.3281789724		},
	{ -0.3722787295,	-21.3007950987	},
	{				0      ,					0				},
	{	-0.3490658504,	-19.9726161263	},
	{	0.0232128791,		1.3281789724		},
	{	-0.3258529713,	-18.6444371539	},
	{	0.0466002928,		2.6663443527		},
	{	-0.3024655594,	-17.3062718735	},
	{	0.0698131701,		3.9945232253		},
	{	-0.2792526803,	-15.9780929011	},
	{	0.0930260474,		5.3227020978		},
	{	-0.2560398013,	-14.6499139287	},
	{	0.1164134629,		6.660867578			},
	{	-0.2326523893,	-13.3117486482	},
	{	0.1396263402,		7.9890464505		},
	{	-0.2094395102,	-11.9835696758	},
	{	0.1628392175,		9.3172253231		},
	{	-0.1862266312,	-10.6553907034	},
	{	0.1862266312,		10.6553907034		}
};

uint16_t concatenate8BigEndian(uint8_t lsb, uint8_t msb){
	return (uint16_t)(msb << 8) | (uint8_t)lsb;
}

lidar_com& lidar_com::init(){

	char filter_exp[] = "port 2368";
	/* Define the device */
	//dev = pcap_lookupdev(errbuf);
	dev = "eth0";
	if (dev == NULL) {
		fprintf(stderr, "Couldn't find default device: %s\n", errbuf);
	}
	/* Find the properties for the device */
	if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1) {
		fprintf(stderr, "Couldn't get netmask for device %s: %s\n", dev, errbuf);
		net = 0;
		mask = 0;
	}
	/* Open the session in promiscuous mode */
	handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
	if (handle == NULL) {
		fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
	}
	/* Compile and apply the filter */
	if (pcap_compile(handle, &fp, filter_exp, 0, net) == -1) {
		fprintf(stderr, "Couldn't parse filter %s: %s\n", filter_exp, pcap_geterr(handle));
	}
	if (pcap_setfilter(handle, &fp) == -1) {
		fprintf(stderr, "Couldn't install filter %s: %s\n", filter_exp, pcap_geterr(handle));
	}

	return *this;
}

lidar_com& lidar_com::getScanPkt(){

	scan_pkt.scans.clear();

	int previous_azimuth, zero_index, start_azm = 0;
	bool zero_cross = false,
	loop = false;

	do{

		data = pcap_next(handle, &header);

		for (int block=0; block<12; block++) {
			block_offset = (block*100)+42; //42 to ignore the ethernet header
			laser_offset = 4;

			current_azimuth = concatenate8BigEndian(data[block_offset+2], data[block_offset+3]);
			scan.azimuth = current_azimuth;

			if (start_azm == 0) {
				start_azm = previous_azimuth = current_azimuth;
				//printf("Start Azimuth %i\n", start_azm);
			}

			for (int laser=0; laser<32; laser++) {
				distance_index = block_offset+laser_offset;
				intensity_index = block_offset+laser_offset + 2;

				point.intensity = data[intensity_index];
				point.distance = 2*concatenate8BigEndian(data[distance_index], data[distance_index+1]);
				point.angle_r = vert_angle[laser][0];
				point.angle_d = vert_angle[laser][1];

				scan.points.push_back(point);
				laser_offset += 3; //next 3 bytes (2 distance, 1 intensity)
			}

			if (!zero_cross && current_azimuth < previous_azimuth) { //Zero Crossing
				zero_cross = true;
				zero_index = scan_pkt.scans.size();
				scan_pkt.scans.push_back(scan);
				scan.points.clear();
				previous_azimuth = current_azimuth;
				//printf("ZERO index: %i\n", zero_index);
				//printf("ZERO azm: %i\n", current_azimuth);
				continue;
			}

			else if (zero_cross && current_azimuth > start_azm) {
				loop = true;
				//printf("FINAL azm: %i\n", current_azimuth);
				break;
			}

			// if (scan_pkt.scans.size() == 2169) {
			// 	scan_pkt.scans.push_back(scan);
			// 	scan.points.clear();
			// 	loop = true;
			// 	break;
			// }

			else {
				scan_pkt.scans.push_back(scan);
				scan.points.clear();
				previous_azimuth = current_azimuth;
				//printf("NEW size: %li\n", scan_pkt.scans.size());
			}
		}
	} while (!loop);

	std::rotate(scan_pkt.scans.begin(), scan_pkt.scans.begin() + zero_index, scan_pkt.scans.end());
	//printf("FINAL size: %li\n", scan_pkt.scans.size());
	return *this;
}
