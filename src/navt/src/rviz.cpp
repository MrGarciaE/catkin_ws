#include <navt/rviz.h>
#include <cstdio>

rviz& rviz::init() {
  vpoints = n.advertise<visualization_msgs::Marker>("points", 1);
	vlines = n.advertise<visualization_msgs::Marker>("lines", 1);
  vregions = n.advertise<visualization_msgs::Marker>("regions", 1);
  zeroPoints = n.advertise<visualization_msgs::Marker>("zeroPoints", 1);
  obstaclePoints = n.advertise<visualization_msgs::Marker>("obstaclePoints", 1);

	points.header.frame_id = line_list.header.frame_id = regions_linelist.header.frame_id = "map";
	points.header.stamp = line_list.header.stamp = regions_linelist.header.stamp = ros::Time::now();
	points.ns = "points";
	line_list.ns = "lines";
  regions_linelist.ns = "regions";
	points.action = line_list.action = regions_linelist.action = visualization_msgs::Marker::ADD;
	points.pose.orientation.w = line_list.pose.orientation.w = regions_linelist.pose.orientation.w = 1.0;
	points.lifetime = line_list.lifetime = regions_linelist.lifetime = ros::Duration();

	line_list.id = 0;
	points.id = 1;
  regions_linelist.id = 2;

	line_list.type = regions_linelist.type = visualization_msgs::Marker::LINE_LIST;
	points.type = visualization_msgs::Marker::POINTS;

	regions_linelist.scale.x = line_list.scale.x = 0.02;
	points.scale.x = 0.01;
	points.scale.y = 0.01;

  return *this;
}

std_msgs::ColorRGBA changeColor() {
  std_msgs::ColorRGBA c;
  c.r = rand() % 256;
  c.g = rand() % 128;
  c.b = rand() % 256;
  c.a = 0.3;

  return c;
}

rviz& rviz::publishLines(navt::line_set &line_stk) {
  navt::point front, back;
  double theta1, theta2, rho1, rho2;

  for (int i = 0; i < line_stk.azm.size(); i++){
    for (int j = 0; j < line_stk.azm[i].line.size(); j++){

      front = line_stk.azm[i].line[j].points.front();
      back = line_stk.azm[i].line[j].points.back();
      theta1 = line_stk.azm[i].line[j].statistics.theta1 * 180/3.14159;
      theta2 = line_stk.azm[i].line[j].statistics.theta2 * 180/3.14159;
      rho1 = line_stk.azm[i].line[j].statistics.rho1;
      rho2 = line_stk.azm[i].line[j].statistics.rho2;
      //printf("[%f,%f]\n", theta1, theta2);
      //printf("[%f,%f]\n", rho1, rho2);

      if (theta1 < 15.0 && theta1 > -15.0) {
        c.r = 0;
        c.g = 1;
        c.b = 0;
        c.a = 0.3;
      }
      else {
        c.r = 1;
        c.g = 0;
        c.b = 0;
        c.a = 0.3;
      }

      //Start point
      p.x = front.x;
      p.y = front.y;
      p.z = front.z;
      //End point
      if(p.x != 0 && p.y != 0 && p.z != 0){
        line_list.points.push_back(p);
        line_list.colors.push_back(c);
        p.x = back.x;
        p.y = back.y;
        p.z = back.z;
        line_list.points.push_back(p);
        line_list.colors.push_back(c);
      }
    }
  }

  vlines.publish(line_list);
  line_list.points.clear();
  line_list.colors.clear();

  return *this;
}

rviz& rviz::publishPoints(navt::scan_pkt &scan_pkt) {
  navt::point point;
  c.r = 0;
  c.g = 0;
  c.b = 1;
  c.a = 0.7;

  for (int i = 0; i < scan_pkt.scans.size(); i++) {
    //printf("%li\n", scan_pkt.scans[i].points.size());
    for (int j = 0; j < scan_pkt.scans[i].points.size(); j++) {
        point = scan_pkt.scans[i].points[j];
        p.x = point.x;
        p.y = point.y;
        p.z = point.z;
        points.points.push_back(p);
        points.colors.push_back(c);
    }
  }
  //std::cout << points << std::endl;
  //while(vpoints.getNumSubscribers() < 1) {}
  //printf("%i\n", vpoints.getNumSubscribers());
  vpoints.publish(points);
  points.points.clear();
  points.colors.clear();
  return *this;
}

rviz& rviz::publishZeroPoints(navt::scan_pkt &scan_pkt) {
  navt::point point;
  c.r = 0;
  c.g = 1;
  c.b = 0;
  c.a = 0.7;

  for (int i = 0; i < scan_pkt.scans.size(); i++) {
    //printf("%li\n", scan_pkt.scans[i].points.size());
    for (int j = 0; j < scan_pkt.scans[i].points.size(); j++) {
        point = scan_pkt.scans[i].points[j];
        p.x = point.x;
        p.y = point.y;
        p.z = point.z;
        points.points.push_back(p);
        points.colors.push_back(c);
    }
  }
  //std::cout << points << std::endl;
  //while(vpoints.getNumSubscribers() < 1) {}
  //printf("%i\n", vpoints.getNumSubscribers());
  zeroPoints.publish(points);
  points.points.clear();
  points.colors.clear();
  return *this;
}

rviz& rviz::publishObstaclePoints(navt::scan_pkt &scan_pkt) {
  navt::point point;
  c.r = 1;
  c.g = 0;
  c.b = 0;
  c.a = 0.7;

  for (int i = 0; i < scan_pkt.scans.size(); i++) {
    //printf("%li\n", scan_pkt.scans[i].points.size());
    for (int j = 0; j < scan_pkt.scans[i].points.size(); j++) {
        point = scan_pkt.scans[i].points[j];
        p.x = point.x;
        p.y = point.y;
        p.z = point.z;
        points.points.push_back(p);
        points.colors.push_back(c);
    }
  }
  //std::cout << points << std::endl;
  //while(vpoints.getNumSubscribers() < 1) {}
  //printf("%i\n", vpoints.getNumSubscribers());
  obstaclePoints.publish(points);
  points.points.clear();
  points.colors.clear();
  return *this;
}


rviz& rviz::publishRegions(navt::region_pkt &closedRegions) {
  navt::point front, back;

  for (int i = 0; i < closedRegions.regions.size(); i++) {
    c = changeColor();

    for (int j = 0; j < closedRegions.regions[i].body.size(); j++) {
      front = closedRegions.regions[i].body[j].points.front();
      back = closedRegions.regions[i].body[j].points.back();
      p.x = front.x;
      p.y = front.y;
      p.z = front.z;
      regions_linelist.points.push_back(p);
      regions_linelist.colors.push_back(c);
      p.x = back.x;
      p.y = back.y;
      p.z = back.z;
      regions_linelist.points.push_back(p);
      regions_linelist.colors.push_back(c);
    }
  }

  vregions.publish(regions_linelist);
  regions_linelist.points.clear();
  regions_linelist.colors.clear();

  return *this;
}
