#include <ros/ros.h>
#include <navt/read_file.h>
#include <navt/line_fit.h>
#include <navt/land_segmentation.h>
#include <navt/rviz.h>
#include <std_msgs/String.h>
#include <geometry_msgs/Twist.h>
#include <string.h>

using namespace std;

string mensaje = "00";
char info_status;
char info_so = '0';
int bandera = 1;

class SubscribeAndPublish{

	public:
		SubscribeAndPublish(){
			lidar_sub = n.subscribe("/lidar_data", 1, &SubscribeAndPublish::lidarCallback, this);
			map3D.init();
		}

		void lidarCallback(navt::scan_pkt lidar_raw_pkt){

			if(info_status=='l')
			{

				bandera = 1;

			}
			else
			{
				bandera = 0;
				mensaje="22";

			}
			bandera=1;
			std::cout << "info status:"<< info_status<<"bandera"<< bandera<< std::endl;
			if(bandera==1){

			line_stk.getLineStock(lidar_raw_pkt);

			//segment.init();
			//segment.getRegions(line_stk.line_set);
			map3D.publishPoints(line_stk.scan_pkt_xyz);
			map3D.publishLines(line_stk.line_set);
			//map3D.publishRegions(segment.closedRegions);
			//ROS_DEBUG("intensities: %i", line_stk.intensitiesId);
			zero_scan_pkt.scans.clear();



			switch(info_so)
			{
				case '0':
					searchingFor = 0;
					break;
				case '1':
					searchingFor = 1;
					break;
				case '2':
					searchingFor = 2;
					break;
				default:
					searchingFor = 0;
					break;
			}


			std::cout <<"searching for"<< searchingFor<< std::endl;


			for(int i=0;i<line_stk.scan_pkt_xyz.scans.size();i++){
					scanTemp.points.clear();
					//unsigned int azm=line_stk.scan_pkt_xyz.scans[i].azimuth;
					scanTemp.azimuth=line_stk.scan_pkt_xyz.scans[i].azimuth;

					for(int j=0;j<line_stk.scan_pkt_xyz.scans[i].points.size();j++){

						if (line_stk.scan_pkt_xyz.scans[i].points[j].angle_d < 0\
								&& line_stk.scan_pkt_xyz.scans[i].points[j].angle_d > -1.4\
								&& line_stk.scan_pkt_xyz.scans[i].points[j].distance!=0){ //searching for the id 13 laser
							/*
							int intensityZero=line_stk.scan_pkt_xyz.scans[i].points[j].intensity;
							std::cout << "intensities: " << intensityZero<< std::endl;
							std::cout << "id: " << line_stk.scan_pkt_xyz.scans[i].points[j].angle_d << std::endl;
							std::cout << "distance: " << line_stk.scan_pkt_xyz.scans[i].points[j].distance << std::endl;
							std::cout << "x: " << line_stk.scan_pkt_xyz.scans[i].points[j].x <<"y: " << line_stk.scan_pkt_xyz.scans[i].points[j].y << std::endl;
							std::cout << "azimuth: " << line_stk.scan_pkt_xyz.scans[i].azimuth << std::endl;
							*/
							scanTemp.points.push_back(line_stk.scan_pkt_xyz.scans[i].points[j]);
							zero_scan_pkt.scans.push_back(scanTemp);
							break;
						}
					}
			}

		/*******************************Average filter*******************************************/
			int numProm=15,flagEntry=0;

			if (flagTemp<=numProm){
				if(flagTemp==0){
					scanPktProm.scans.clear();
				}
				for(int i=0;i<zero_scan_pkt.scans.size();i++){

					int intensityProm=zero_scan_pkt.scans[i].points[0].intensity;
					int distanceProm=zero_scan_pkt.scans[i].points[0].distance;
					float xProm=zero_scan_pkt.scans[i].points[0].x;
					float yProm=zero_scan_pkt.scans[i].points[0].y;
					float zProm=zero_scan_pkt.scans[i].points[0].z;


					if(flagTemp==0){

						scanProm.azimuth=zero_scan_pkt.scans[i].azimuth;

						scanProm.points.push_back(zero_scan_pkt.scans[i].points[0]),
						scanPktProm.scans.push_back(scanProm);
						scanProm.points.clear();

					}
					else{


						for(int j=0;j<scanPktProm.scans.size();j++){
							if(zero_scan_pkt.scans[i].azimuth<=scanPktProm.scans[j].azimuth+15\
									&& zero_scan_pkt.scans[i].azimuth>=scanPktProm.scans[j].azimuth-15\
									&& flagTemp<numProm){

								scanPktProm.scans[j].points[0].intensity=scanPktProm.scans[j].points[0].intensity+intensityProm;
								scanPktProm.scans[j].points[0].distance=scanPktProm.scans[j].points[0].distance+distanceProm;
								scanPktProm.scans[j].points[0].x=scanPktProm.scans[j].points[0].x+xProm;
								scanPktProm.scans[j].points[0].y=scanPktProm.scans[j].points[0].y+yProm;
								scanPktProm.scans[j].points[0].z=scanPktProm.scans[j].points[0].z+zProm;
							}
							else if(flagTemp==numProm){

								scanPktProm.scans[j].points[0].intensity=scanPktProm.scans[j].points[0].intensity/numProm;
								scanPktProm.scans[j].points[0].distance=scanPktProm.scans[j].points[0].distance/numProm;
								scanPktProm.scans[j].points[0].x=scanPktProm.scans[j].points[0].x/numProm;
								scanPktProm.scans[j].points[0].y=scanPktProm.scans[j].points[0].y/numProm;
								scanPktProm.scans[j].points[0].z=scanPktProm.scans[j].points[0].z/numProm;
								flagTemp=0;
								flagEntry=1;
								scanPktProm.scans.clear();
							}
						}
					}
				}
			}
			flagTemp++;

		if(flagEntry==1){
		/****************code for detection of markers****************************/
			/*************discriminating by intensity******************/
			scanCilinders.scans.clear();
			scanObstacles.scans.clear();
			int changeInt=2;
			int flag=0;
			int countCil=0;
			int lastIntensityCil=scanPktProm.scans[scanPktProm.scans.size()-1].points[0].intensity;
			int difIntensity;
			int prevIntensity;

			for(int i=0;i<scanPktProm.scans.size();i++){
				scanTemp.points.clear();

				for(int j=0;j<scanPktProm.scans[i].points.size();j++){
					int intensityCil=scanPktProm.scans[i].points[j].intensity;
					if(scanPktProm.scans[i].points[j].distance >500 && intensityCil!=0 && scanPktProm.scans[i].points.size()!=0){
						if(i==0){
							difIntensity=intensityCil-lastIntensityCil;
							prevIntensity=intensityCil;

						}
						else{
							difIntensity=intensityCil-prevIntensity;
							prevIntensity=intensityCil;
						}
						if(difIntensity<-40 && !(intensityCil>45 && intensityCil<=105) && flag==1){
							flag=0;

						}
						if ((difIntensity>changeInt || flag==1)&& intensityCil>45 && intensityCil<=105){//intensity markers
							scanTemp.points.push_back(scanPktProm.scans[i].points[j]);
							scanTemp.azimuth=scanPktProm.scans[i].azimuth;
							flag=1;
							scanCilinders.scans.push_back(scanTemp);
						}
						else if((difIntensity>changeInt || flag==1)&& intensityCil>5 && intensityCil<=44){//intensity obst
							scanTemp.points.push_back(zero_scan_pkt.scans[i].points[j]);
							scanTemp.azimuth=zero_scan_pkt.scans[i].azimuth;
							//flag=1;
							scanObstacles.scans.push_back(scanTemp);
						}
						if(flagTemp==1){
								int  intenTemp=scanTemp.points[j].intensity;
								std::cout << "intensities(point:"<< i<<",laser:"<< j<<"): "<< intenTemp<< std::endl;//puntos despues del algoritmo de cilindros
								//std::cout << "intensities(point:"<< i<<",laser:"<< j<<"): "<< intensityCil<< std::endl;//puntos antes del algotmo de cilindros
						}


					}
				}
			}


			/*************discriminating by shape****************/
			scanPktTemp.scans.clear();
			int prevAzimuth;
			int actualAzimuth;
			int difAzimuth;
			int sizeList=0;
			int minDistance,maxDistance;
			int minIndex, maxIndex;
			int neigh1=-1;
			int neigh2=-1;
			int randN;
			int countPointNoFix=0;
			float porcentualFix, porcent=0.01;//10%
			float mAB,mBC,pX,pY,yA,xA,xB,yB,xC,yC,distCenter,slack=0.02;//1cm holgura
			if(searchingFor==1){
				porcent=0.3;
				//slack=0.025;
				slack=0.03;
			}

			flag=0;


			scanCiliderPkt.scans.clear();

			for(int i=0;i<scanCilinders.scans.size();i++){
				scanTemp.points.clear();

				actualAzimuth=scanCilinders.scans[i].azimuth;
				if(i==0){
					prevAzimuth=scanCilinders.scans[scanCilinders.scans.size()-1].azimuth;
					difAzimuth=actualAzimuth-prevAzimuth;
				}
				else{
					prevAzimuth=scanCilinders.scans[i-1].azimuth;
					difAzimuth=actualAzimuth-prevAzimuth;
				}

				if(difAzimuth<20){

					sizeList++;
					if(sizeList==1){

						if(i==0){

							scanTemp.points.push_back(scanCilinders.scans[scanCilinders.scans.size()-1].points[0]);

							scanTemp.azimuth=scanCilinders.scans[scanCilinders.scans.size()-1].azimuth;

						}
						else{
							scanTemp.points.push_back(scanCilinders.scans[i-1].points[0]);
							scanTemp.azimuth=scanCilinders.scans[i-1].azimuth;

						}

						scanPktTemp.scans.push_back(scanTemp);
						scanTemp.points.clear();

					}

					scanTemp.points.push_back(scanCilinders.scans[i].points[0]);

					scanTemp.azimuth=scanCilinders.scans[i].azimuth;

					scanPktTemp.scans.push_back(scanTemp);

				}

				else{

					if(sizeList<8){//Minimum 6 points to analyze figure
						scanPktTemp.scans.clear();
						sizeList=0;
					}
					else{

						neigh2=-1;
						neigh1=-1;

						for (int k=0;k<scanPktTemp.scans.size();k++){

							if(k==0){
								minDistance=scanPktTemp.scans[k].points[0].distance;
								maxDistance=scanPktTemp.scans[k].points[0].distance;
								minIndex=k;
								neigh1=k;
							}
							if(scanPktTemp.scans[k].points[0].distance<minDistance){
								minDistance=scanPktTemp.scans[k].points[0].distance;
								minIndex=k;
							}
							if(scanPktTemp.scans[k].points[0].distance>maxDistance){
								maxDistance=scanPktTemp.scans[k].points[0].distance;
								neigh1=k;
							}
						}

						//if(minDistance==maxDistance){
							neigh1=-1;
							while(1){
								randN=rand() % scanPktTemp.scans.size() + 0;

								if(randN!=minIndex\
										&& (randN==minIndex-1 || randN!=minIndex+1)){
									minIndex=randN;
									break;
								}
							}
							if(searchingFor==1){
								neigh1=rand() % 3 + 1;
								neigh2=scanPktTemp.scans.size()-2;
							}
							if(searchingFor==2){
								neigh1=rand() % 4 + 2;
								neigh2=scanPktTemp.scans.size()-4;
							}


						xA=scanPktTemp.scans[neigh1].points[0].x;
						xB=scanPktTemp.scans[minIndex].points[0].x;
						xC=scanPktTemp.scans[neigh2].points[0].x;
						yA=scanPktTemp.scans[neigh1].points[0].y;
						yB=scanPktTemp.scans[minIndex].points[0].y;
						yC=scanPktTemp.scans[neigh2].points[0].y;
						std::cout << "afsdjkhfasdjjfasjhkdfasdjhkfjklasdfajklsdfjhlkasdfjkl"<< std::endl;
						if((yA-yB)==0 || (yB-yC)==0 || (xA-xB)==0 || (xB-xC)==0 || neigh1==neigh2 || neigh1==minIndex\
								||neigh2==minIndex){
							scanPktTemp.scans.clear();
						}
						else{

							if(searchingFor==0){//0 for nothing, 1 for smallMarker, 2 for mediumMarker
								flagSmall=1;
								flagBig=1;
								flagMedium=1;
							}

							mAB=(yA-yB)/(xA-xB);
							mBC=(yB-yC)/(xB-xC);
							pX=(mAB*mBC*(yC-yA)+mAB*(xC+xB)-mBC*(xA+xB))/(2*(mAB-mBC));
							pY=((pX-((xA+xB)/2))/mAB)+((yA+yB)/2);
							distCenter=sqrt(pow((xB-pX),2)+pow((yB-pY),2));

							if(distCenter==distCenter && distCenter>0.03 && distCenter<0.07 && flagSmall!=1\
									&& searchingFor==1){//&& maxDistance>=minDistance+20){
								std::cout << "distance Center: " << distCenter<< std::endl;
								porcentualFix=scanPktTemp.scans.size()*porcent;
								countPointNoFix=0;
								for(int k=0;k<scanPktTemp.scans.size();k++){

									if(sqrt(pow((scanPktTemp.scans[k].points[0].x-pX),2)+\
										pow((scanPktTemp.scans[k].points[0].y-pY),2))>distCenter+slack\
										|| sqrt(pow((scanPktTemp.scans[k].points[0].x-pX),2)+\
										pow((scanPktTemp.scans[k].points[0].y-pY),2))<distCenter-slack){

											countPointNoFix++;
											if(countPointNoFix>porcentualFix){
												scanPktTemp.scans.clear();
											}
									}
									std::cout <<"distancia punto "<<k<<": " <<sqrt(pow((scanPktTemp.scans[k].points[0].x-pX),2)+\
									pow((scanPktTemp.scans[k].points[0].y-pY),2))<< std::endl;
								}
								if (scanPktTemp.scans.size()!=0){
									flagSmall=1;
								}
							}
							else if(distCenter==distCenter && distCenter>0.17 && distCenter<0.23 && flagMedium!=1\
									&& searchingFor==2){
								std::cout << "distance Center: " << distCenter<< std::endl;
								porcentualFix=scanPktTemp.scans.size()*porcent;
								countPointNoFix=0;
								if(scanPktTemp.scans.size()>=30){
									porcentualFix=scanPktTemp.scans.size()*0.9;
								}
								for(int k=0;k<scanPktTemp.scans.size();k++){

									if(sqrt(pow((scanPktTemp.scans[k].points[0].x-pX),2)+\
										pow((scanPktTemp.scans[k].points[0].y-pY),2))>distCenter+slack\
										|| sqrt(pow((scanPktTemp.scans[k].points[0].x-pX),2)+\
										pow((scanPktTemp.scans[k].points[0].y-pY),2))<distCenter-slack){

											countPointNoFix++;
											if(countPointNoFix>porcentualFix){
												scanPktTemp.scans.clear();
											}
									}
									std::cout <<"distancia punto "<<k<<": " <<sqrt(pow((scanPktTemp.scans[k].points[0].x-pX),2)+\
									pow((scanPktTemp.scans[k].points[0].y-pY),2))<< std::endl;
								}
								if (scanPktTemp.scans.size()!=0){
									flagMedium=1;
								}
							}

							else{

								scanPktTemp.scans.clear();
							}
						}
						if(scanPktTemp.scans.size()!=0){

							std::cout <<"entrooooooooooooooooooooooo"<<std::endl;
						}
						for(int j=0;j<scanPktTemp.scans.size();j++){
							std::cout << "distance: " << scanPktTemp.scans[j].points[0].distance << std::endl;
							std::cout << "x: " << scanPktTemp.scans[j].points[0].x << std::endl;
							std::cout << "y: " << scanPktTemp.scans[j].points[0].y << std::endl;
							std::cout << "azimuth: " << scanPktTemp.scans[j].azimuth << std::endl;
							if(flagMedium==1 && searchingFor==2){
								scanCiliderPkt.scans.push_back(scanPktTemp.scans[j]);

							}
							else if(flagSmall==1 && searchingFor==1){
								scanCiliderPkt.scans.push_back(scanPktTemp.scans[j]);
							}

						}
						sizeList=0;
						scanPktTemp.scans.clear();
					}

				}

			}

			if((flagMedium!=1 && searchingFor==2)|| (flagSmall!=1 && searchingFor==1)){
			/*************discriminating by shape obstacle****************/
				scanPktTemp.scans.clear();
				int prevAzimuth;
				int actualAzimuth;
				int difAzimuth;
				int sizeList=0;
				int minDistance,maxDistance;
				int minIndex, maxIndex;
				int neigh1=-1;
				int neigh2=-1;
				int randN;
				int countPointNoFix=0;
				float porcentualFix, porcent=0.1;//10%
				float mAB,mBC,pX,pY,yA,xA,xB,yB,xC,yC,distCenter,slack=0.03;//1cm holgura


				flag=0;


				scanObstaclePkt.scans.clear();

				for(int i=0;i<scanObstacles.scans.size();i++){
					scanTemp.points.clear();

					actualAzimuth=scanObstacles.scans[i].azimuth;
					if(i==0){
						prevAzimuth=scanObstacles.scans[scanObstacles.scans.size()-1].azimuth;
						difAzimuth=actualAzimuth-prevAzimuth;
					}
					else{
						prevAzimuth=scanObstacles.scans[i-1].azimuth;
						difAzimuth=actualAzimuth-prevAzimuth;
					}

					if(difAzimuth<20){

						sizeList++;
						if(sizeList==1){

							if(i==0){

								scanTemp.points.push_back(scanObstacles.scans[scanObstacles.scans.size()-1].points[0]);

								scanTemp.azimuth=scanObstacles.scans[scanObstacles.scans.size()-1].azimuth;

							}
							else{
								scanTemp.points.push_back(scanObstacles.scans[i-1].points[0]);
								scanTemp.azimuth=scanObstacles.scans[i-1].azimuth;

							}

							scanPktTemp.scans.push_back(scanTemp);
							scanTemp.points.clear();

						}

						scanTemp.points.push_back(scanObstacles.scans[i].points[0]);

						scanTemp.azimuth=scanObstacles.scans[i].azimuth;

						scanPktTemp.scans.push_back(scanTemp);

					}

					else{

						if(sizeList<8){//Minimum 6 points to analyze figure
							scanPktTemp.scans.clear();
							sizeList=0;
						}
						else{

							neigh2=-1;
							neigh1=-1;
							for (int k=0;k<scanPktTemp.scans.size();k++){
								if(k==0){
									minDistance=scanPktTemp.scans[k].points[0].distance;
									maxDistance=scanPktTemp.scans[k].points[0].distance;
									minIndex=k;
									neigh1=k;
								}
								if(scanPktTemp.scans[k].points[0].distance<minDistance){
									minDistance=scanPktTemp.scans[k].points[0].distance;
									minIndex=k;
								}
								if(scanPktTemp.scans[k].points[0].distance>maxDistance){
									maxDistance=scanPktTemp.scans[k].points[0].distance;
									neigh1=k;
								}
							}
							/****************INDEX min and max*********/
								neigh1=-1;
								while(1){
									randN=rand() % scanPktTemp.scans.size() + 0;

									if(randN!=minIndex\
											&& (randN==minIndex-1 || randN!=minIndex+1)){
										minIndex=randN;
										break;
									}
								}

								neigh1=rand() % 4 + 2;
								neigh2=scanPktTemp.scans.size()-4;


							xA=scanPktTemp.scans[neigh1].points[0].x;
							xB=scanPktTemp.scans[minIndex].points[0].x;
							xC=scanPktTemp.scans[neigh2].points[0].x;
							yA=scanPktTemp.scans[neigh1].points[0].y;
							yB=scanPktTemp.scans[minIndex].points[0].y;
							yC=scanPktTemp.scans[neigh2].points[0].y;
							if((yA-yB)==0 || (yB-yC)==0 || (xA-xB)==0 || (xB-xC)==0 || neigh1==neigh2 || neigh1==minIndex\
									||neigh2==minIndex){
								scanPktTemp.scans.clear();
							}
							else{
								if(searchingFor==0){//0 for nothing, 1 for smallMarker, 2 for mediumMarker
									flagSmall=1;
									flagBig=1;
									flagMedium=1;
								}

								mAB=(yA-yB)/(xA-xB);
								mBC=(yB-yC)/(xB-xC);
								pX=(mAB*mBC*(yC-yA)+mAB*(xC+xB)-mBC*(xA+xB))/(2*(mAB-mBC));
								pY=((pX-((xA+xB)/2))/mAB)+((yA+yB)/2);
								distCenter=sqrt(pow((xB-pX),2)+pow((yB-pY),2));

								if(distCenter==distCenter && distCenter>0.13 && distCenter<0.27 && flagBig!=1\
										&& searchingFor!=0){
									std::cout << "distance Center: " << distCenter<< std::endl;
									porcentualFix=scanPktTemp.scans.size()*porcent;
									if(scanPktTemp.scans.size()>=35){
										porcentualFix=scanPktTemp.scans.size()*0.9;
									}
									countPointNoFix=0;
									for(int k=0;k<scanPktTemp.scans.size();k++){

										if(sqrt(pow((scanPktTemp.scans[k].points[0].x-pX),2)+\
											pow((scanPktTemp.scans[k].points[0].y-pY),2))>distCenter+slack\
											|| sqrt(pow((scanPktTemp.scans[k].points[0].x-pX),2)+\
											pow((scanPktTemp.scans[k].points[0].y-pY),2))<distCenter-slack){

												countPointNoFix++;
												if(countPointNoFix>porcentualFix){
													scanPktTemp.scans.clear();
												}
										}
										std::cout <<"distancia punto "<<k<<": " <<sqrt(pow((scanPktTemp.scans[k].points[0].x-pX),2)+\
										pow((scanPktTemp.scans[k].points[0].y-pY),2))<< std::endl;
									}
									if (scanPktTemp.scans.size()!=0){
										flagBig=1;
									}
								}
								else{

									scanPktTemp.scans.clear();
								}
							}
							if(scanPktTemp.scans.size()!=0){

								std::cout <<"entrooooooooooooooooooooooo rojo"<<std::endl;
							}
							for(int j=0;j<scanPktTemp.scans.size();j++){
								std::cout << "distance: " << scanPktTemp.scans[j].points[0].distance << std::endl;
								std::cout << "x: " << scanPktTemp.scans[j].points[0].x << std::endl;
								std::cout << "y: " << scanPktTemp.scans[j].points[0].y << std::endl;
								std::cout << "azimuth: " << scanPktTemp.scans[j].azimuth << std::endl;
								if(flagBig==1 && searchingFor!=0){
									scanObstaclePkt.scans.push_back(scanPktTemp.scans[j]);
								}
							}
							sizeList=0;
							scanPktTemp.scans.clear();
						}

					}

				}

			}
		/***************Demo martes robotica******************************/
			/*if(flagTemp==500){
				scanPktTemp.scans.clear();
				flagTemp=0;
			}
		    scanTemp.points.clear();
			int azmAllLaser;
			int azmOneLaser;
			for(int i=0;i<line_stk.scan_pkt_xyz.scans.size();i++){
				for(int j=0;j<scanCiliderPkt.scans.size();j++){

					azmAllLaser=line_stk.scan_pkt_xyz.scans[i].azimuth;
					azmOneLaser=scanCiliderPkt.scans[j].azimuth;

					if(azmAllLaser==azmOneLaser){
						for(int k=0;k<line_stk.scan_pkt_xyz.scans[i].points.size();k++){
							if(line_stk.scan_pkt_xyz.scans[i].points[k].angle_d < 0 &&\
							 line_stk.scan_pkt_xyz.scans[i].points[k].angle_d > -5.4){
								 scanTemp.points.clear();
								 scanTemp.points.push_back(line_stk.scan_pkt_xyz.scans[i].points[k]);
							 	 scanPktTemp.scans.push_back(scanTemp);
							 }
						}

					}
				}
			}
			map3D.publishZeroPoints(scanPktTemp);*/
		/**********************************Rviz***************************************/

			//map3D.publishZeroPoints(scanCilinders);//intensities
			map3D.publishZeroPoints(scanCiliderPkt);//intensities + shape
			//map3D.publishZeroPoints(zero_scan_pkt); //all laser 13
			//map3D.publishZeroPoints(scanPktProm);//promediador
			//map3D.publishObstaclePoints(scanObstacles);//obstacles with intensities filter
			//map3D.publishObstaclePoints(scanObstaclePkt);//obstacles intensities + shape filter
			map3D.publishObstaclePoints(scanCilinders);//obstacles intensities + shape filter
		if(attempts==10 || (flagMedium==1 && searchingFor==2) || (flagSmall==1 && searchingFor==1)){
		/***************************************navigation*****************************************/
			attempts=0;
			/*+++++++++++++++searching for Medium marker++++++++++++++++++++++++*/
			if(flagMedium==1 && searchingFor==2){//0 for nothing, 1 for smallMarker, 2 for mediumMarker
				//Algorithm for going to marker Medium
					/****taking min distance and max distance****/
				for (int k=0;k<scanCiliderPkt.scans.size();k++){
					if(k==0){
						minDistance=scanCiliderPkt.scans[k].points[0].distance;
						maxDistance=scanCiliderPkt.scans[k].points[0].distance;
						minIndex=k;
						maxIndex=k;
					}
					if(scanCiliderPkt.scans[k].points[0].distance<minDistance){
						minDistance=scanCiliderPkt.scans[k].points[0].distance;
						minIndex=k;
					}
					if(scanCiliderPkt.scans[k].points[0].distance>maxDistance){
						maxDistance=scanCiliderPkt.scans[k].points[0].distance;
						maxIndex=k;
					}
				}
				/****Orientation and forwarding signal send to jaguar****/
				if(minDistance>1000){
					if(scanCiliderPkt.scans[minIndex].azimuth>9300 &&\
							scanCiliderPkt.scans[minIndex].azimuth<27000){
						mensaje = "01";
					}
					else if((scanCiliderPkt.scans[minIndex].azimuth>=27000 &&\
							scanCiliderPkt.scans[minIndex].azimuth<36000) ||\
							(scanCiliderPkt.scans[minIndex].azimuth<8800 &&\
							scanCiliderPkt.scans[minIndex].azimuth>=0)){
						mensaje = "10";
					}
					else{
						mensaje = "11";
					}
				}
				else{
					mensaje = "00";
				}
				//reseting flags
				flagSmall=0;
				flagBig=0;
				flagMedium=0;
			}
			/*+++++++++++++++searching for small marker++++++++++++++++++++++++*/
			else if(flagSmall==1 && searchingFor==1){
				//Algorithm for going to marker Small

					/****taking min distance and max distance****/
				for (int k=0;k<scanCiliderPkt.scans.size();k++){
					if(k==0){
						minDistance=scanCiliderPkt.scans[k].points[0].distance;
						maxDistance=scanCiliderPkt.scans[k].points[0].distance;
						minIndex=k;
						maxIndex=k;
					}
					if(scanCiliderPkt.scans[k].points[0].distance<minDistance){
						minDistance=scanCiliderPkt.scans[k].points[0].distance;
						minIndex=k;
					}
					if(scanCiliderPkt.scans[k].points[0].distance>maxDistance){
						maxDistance=scanCiliderPkt.scans[k].points[0].distance;
						maxIndex=k;
					}
				}
				/****Orientation and forwarding signal send to jaguar****/
				if(minDistance>1000){
					if(scanCiliderPkt.scans[minIndex].azimuth>9200 &&\
							scanCiliderPkt.scans[minIndex].azimuth<27000){
						mensaje = "01";//derecha
						std::cout << "derecha"<<mensaje<< std::endl;

					}
					else if((scanCiliderPkt.scans[minIndex].azimuth>=27000 &&\
							scanCiliderPkt.scans[minIndex].azimuth<36000) ||\
							(scanCiliderPkt.scans[minIndex].azimuth<8800 &&\
							scanCiliderPkt.scans[minIndex].azimuth>=0)){
						mensaje = "10";//izquierda
						std::cout << "izquierda"<<mensaje<< std::endl;
					}
					else{
						mensaje = "11";//avanzar
						std::cout << "avanzar"<<mensaje<< std::endl;
					}
				}
				else{
					mensaje = "00";//detenerse llegada
					std::cout << "detenerse"<<mensaje<< std::endl;
				}
				//reseting flags
				flagSmall=0;
				flagBig=0;
				flagMedium=0;
			}
			/*+++++++++++++++searching for big marker++++++++++++++++++++++++*/
			else if(flagBig==1 && searchingFor!=0){
				//Algorithm for going to big marker
					/****taking min distance and max distance****/
				for (int k=0;k<scanObstacles.scans.size();k++){
					if(k==0){
						minDistance=scanObstacles.scans[k].points[0].distance;
						maxDistance=scanObstacles.scans[k].points[0].distance;
						minIndex=k;
						maxIndex=k;
					}
					if(scanObstacles.scans[k].points[0].distance<minDistance){
						minDistance=scanObstacles.scans[k].points[0].distance;
						minIndex=k;
					}
					if(scanObstacles.scans[k].points[0].distance>maxDistance){
						maxDistance=scanObstacles.scans[k].points[0].distance;
						maxIndex=k;
					}
				}
					/****Orientation and forwarding signal send to jaguar****/
				if(minDistance>1000){
					if(scanObstacles.scans[minIndex].azimuth>9300 &&\
							scanObstacles.scans[minIndex].azimuth<27000){
						mensaje = "01";
					}
					else if((scanCiliderPkt.scans[minIndex].azimuth>=27000 &&\
							scanCiliderPkt.scans[minIndex].azimuth<36000) ||\
							(scanCiliderPkt.scans[minIndex].azimuth<8800 &&\
							scanCiliderPkt.scans[minIndex].azimuth>=0)){
						mensaje = "10";
					}
					else{
						mensaje = "11";
					}
				}
				else{
					//rotating around obstacle
					if(scanObstacles.scans[minIndex].azimuth>18300 &&\
							scanObstacles.scans[minIndex].azimuth<35999){
						mensaje = "01";
					}
					else if(scanObstacles.scans[minIndex].azimuth>=0 &&\
							scanObstacles.scans[minIndex].azimuth<17700){
						mensaje = "10";
					}
					else{
						mensaje = "11";
					}
				}
				//reseting flags
				flagSmall=0;
				flagBig=0;
				flagMedium=0;
			}
		/*************************************************************************************************/
			flagSmall=0;
			flagBig=0;
			flagMedium=0;
			}
			attempts++;
			}
		}

	}

	private:
		ros::NodeHandle n;
		ros::Subscriber lidar_sub;
		line_fit line_stk;
		//land_segmentation segment;
		rviz map3D;
		navt::scan_pkt zero_scan_pkt;
		navt::scan scanTemp;
		navt::scan scanProm;
		navt::scan_pkt scanCilinders;
		navt::scan_pkt scanCiliderPkt;
		navt::scan_pkt scanObstaclePkt;
		navt::scan_pkt scanObstacles;
		navt::scan_pkt scanPktTemp;
		navt::scan_pkt scanPktProm;
		int flagTemp=0, attempts=0;
		int searchingFor;//0 for nothing, 1 for smallMarker, 2 for mediumMarker
		int flagBig=0, flagMedium=0, flagSmall=0;
		int flagTest=0;
};

void chatterCallbackM(const std_msgs::String::ConstPtr& msg)
{
  string temp = msg->data.c_str();
  info_status = temp[0];
}

void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
  string temp = msg->data.c_str();
  info_so = temp[0];
}

int main(int argc, char **argv){

	ros::init(argc, argv, "navt_main");

	SubscribeAndPublish SAPObject;
	ros::NodeHandle n;
	ros::Publisher chatter_pub = n.advertise<std_msgs::String>("insvel", 1000);
	ros::Rate loop_rate(10);

	ros::NodeHandle ns;
	ros::Subscriber sub = ns.subscribe("statuso", 1000, chatterCallback);

	ros::NodeHandle nm;
	ros::Subscriber subm = nm.subscribe("status", 1000, chatterCallbackM);


	while (ros::ok())
	{
		std_msgs::String msg;

		std::stringstream ss;
		ss << mensaje << endl;
		msg.data = ss.str();

		chatter_pub.publish(msg);

		ros::spinOnce();
		loop_rate.sleep();
	}

	return 0;
}
