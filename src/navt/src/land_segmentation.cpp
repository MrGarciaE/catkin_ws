#include <navt/land_segmentation.h>
#include <cstdio>

#define PI 3.14159
#define MIN_REGION_LINES 5

bool overlap(navt::line lastLine, navt::line newLine) {
  bool crossCheck = (lastLine.points.front().u < newLine.points.back().u)
  && (lastLine.points.back().u > newLine.points.front().u);
  return crossCheck;
}

land_segmentation& land_segmentation::updateRegionParameters(navt::region &region) {
  return *this;
}

land_segmentation& land_segmentation::newRegion(navt::line newLine) {
  navt::region region;
  region.grow = true;
  region.body.push_back(newLine);
  land_segmentation::updateRegionParameters(region);
  possibleRegions.regions.push_back(region);
  return *this;
}

land_segmentation& land_segmentation::newObstacle(navt::line newLine) {

  //Add line to possibleObstacles
  return *this;
}

land_segmentation& land_segmentation::growRegion(navt::region_pkt &possibleRegions,
  int r, navt::line newLine, int fusion) {
  if (fusion == 1) {
    if (!possibleRegions.regions[r].grow) { //Simple attach
      possibleRegions.regions[r].body.push_back(newLine);
      updateRegionParameters(possibleRegions.regions[r]);
    }
    else { //It branches
      // navt::branch br;
      // br.sub_region.push_back(newLine);
      // possibleRegions.regions[r].branches.push_back(br);
    }
  }
  else {

    //It attaches 'fusion' number of regions
  }
  possibleRegions.regions[r].grow = true;
  return *this;
}

land_segmentation& land_segmentation::closeRegion(int r) {
  if (possibleRegions.regions[r].body.size() > MIN_REGION_LINES){
    if (r == -1) {
      for (int i = 0; i < possibleRegions.regions.size(); i++) {
        closedRegions.regions.push_back(possibleRegions.regions[i]);
        possibleRegions.regions.clear();
      }
    }
    else {
      closedRegions.regions.push_back(possibleRegions.regions[r]);
      possibleRegions.regions.erase(possibleRegions.regions.begin() + r);
    }
  }
  else {
    possibleRegions.regions.erase(possibleRegions.regions.begin() + r);
  }
  return *this;
}

land_segmentation& land_segmentation::init(navt::azm firstAzm) {
// ----------------------------------------//
  double tilt;

//------------------------------------------------------------//
  for (int i = 0; i < firstAzm.line.size(); i++) {
    tilt = firstAzm.line[i].statistics.theta1 * 180/PI;
    if (tilt < 15 && tilt > -15) {
      land_segmentation::newRegion(firstAzm.line[i]);
    }
    else {
      land_segmentation::newObstacle(firstAzm.line[i]);
    }
  }
//--------------------------------------------------------------//
  return *this;
}

land_segmentation& land_segmentation::getRegions(navt::line_set line_set){
  navt::azm newAzm;
  navt::line newLine, lastLine;
  double theta, theta_prev, rho, rho_prev, tilt;
  int fusion, init_regions;

  closedRegions.regions.clear();
  possibleRegions.regions.clear();
  land_segmentation::init(line_set.azm[0]);

//-------------------------------------------------------------//
  for (int scan = 1; scan < line_set.azm.size(); scan++) {
    newAzm = line_set.azm[scan];

    init_regions = possibleRegions.regions.size();
    for (int r = 0; r < init_regions; r++) {
      possibleRegions.regions[r].grow = false;
    }
    //printf("[%i] Start possible regions: %li\n", scan, possibleRegions.regions.size());

    for (int i = 0; i < newAzm.line.size(); i++) {

      newLine = newAzm.line[i];
      theta = newLine.statistics.theta1 * 180/PI;
      rho = newLine.statistics.rho1;
      fusion = 0;

      if (theta < 15 && theta > -15) {
        for (int j = 0; j < init_regions; j++) {
          //LastAzm para poder iterar sobre las ultimas lineas de la region
          lastLine = possibleRegions.regions[j].body.back();
          theta_prev = lastLine.statistics.theta1 * 180/PI;
          rho_prev = lastLine.statistics.rho1;

          if (lastLine.identity.numof_scan + 1 == newLine.identity.numof_scan) {
            if (fabs(theta_prev - theta) < 1) {
              if (fabs(rho_prev - rho) < 1) {
                if (overlap(lastLine, newLine)) {
                  fusion++;
                  land_segmentation::growRegion(possibleRegions, j, newLine, fusion);
                }
              }
            }
          }
        }
        if (fusion == 0) {
          land_segmentation::newRegion(newLine);
        }
      }
    }

    for (int r = 0; r < possibleRegions.regions.size(); r++) {
      if (!possibleRegions.regions[r].grow)
        land_segmentation::closeRegion(r);
    }
    //printf("[%i] End possible regions: %li\n", scan, possibleRegions.regions.size());
    //printf("---------------------------------\n");
  }
  land_segmentation::closeRegion(-1);
  return *this;
}
