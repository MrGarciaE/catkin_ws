#include <ros/ros.h>
#include <navt/lidar_com.h>

int main(int argc, char **argv){

	ros::init(argc, argv, "lidar_main");
  ros::NodeHandle n;
  ros::Publisher lidar_pub = n.advertise<navt::scan_pkt>("lidar_data", 1);

	lidar_com lidar;
	lidar.init();

	while (ros::ok()){
		lidar.getScanPkt();
    lidar_pub.publish(lidar.scan_pkt);
		ros::spinOnce();
	}

	return 0;
}
