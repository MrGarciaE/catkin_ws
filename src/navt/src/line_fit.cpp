#include <navt/line_fit.h>
#include <cmath>
#include <algorithm>
#include <cstdio>



#define PI 3.14159
#define POINT_RADIUS 4.0 	//Scenario points radius in meters
#define LINE_RADIUS 100.0 //Radius (in meters) to be analized by the line fitting algorithm
#define LINE_MIN_VIEW_ANGLE 169 //Angle in degrees [0 - 360] cone = 86
#define LINE_MAX_VIEW_ANGLE 171 //Angle in degrees [0 - 360] must be > LINE_RANGE_MIN cone = 94

// Node for Autonomous Navigation of Land Vehicles

//Boolean condition to sort rows from a 2D array, comparing only the first column
bool angleCmp(const navt::point &p1, const navt::point &p2){
	return p1.angle_r < p2.angle_r;
}

//Function to sort rows by increasing vertical angle
void sort_angle(navt::scan &scan){
	std::sort(scan.points.begin(), scan.points.end(), angleCmp);
}

//Function to transform from 3D spherical coordinates (rho, phi, theta) to 3D cartesian coordinates (x, y, z)
void spherical2cartesian(navt::scan &scan){
	double rho, phi, theta = (double)scan.azimuth/100;

	for (int i=0; i<scan.points.size(); i++){
		rho = (double)scan.points[i].distance/1000;
		phi = scan.points[i].angle_r;
		//printf("rho,phi,theta : [%f,%f,%f]\n", rho, phi, theta);
		if(rho < POINT_RADIUS){
			scan.points[i].id = i;
			scan.points[i].x = rho*sin(PI/2 - phi) * cos(PI/2-theta * PI/180);
			scan.points[i].y = rho*sin(PI/2 - phi) * sin(PI/2-theta * PI/180);
			scan.points[i].z = rho*cos(PI/2 - phi);
			//printf("x,y,z : [%f,%f,%f]\n", scan.points[i].x,scan.points[i].y,scan.points[i].z);

		}
	}
}

//Function to obtain the 2D cartesian coordinates (u,v) projected on the corresponding azimuth step
void UV_coord(navt::scan &scan){
	for (int i=0; i<scan.points.size(); i++){
		if((double)scan.points[i].distance/1000 < LINE_RADIUS){
			scan.points[i].u = sqrt(scan.points[i].x * scan.points[i].x +
				scan.points[i].y * scan.points[i].y);
			scan.points[i].v = scan.points[i].z;
		}
	}
}

//Function to obtain the euclidean distance between two points
double dist_btw_points(navt::point a, navt::point b){
	double rX = b.u - a.u;
	double rY = b.v - a.v;
	return sqrt(rX * rX + rY * rY);
}

//Function to obtain the angle between two vectors defined by a set of 3 points (vector 'a-b' and vector 'b-c')
double angle_btw_vectors(navt::point a, navt::point b, navt::point c){

	double A[2], B[2];
	double angle;
	double d1,d2;
	d1 = dist_btw_points(a,b);
	d2 = dist_btw_points(b,c);

	A[0] = a.u - b.u;
	A[1] = a.v - b.v;

	B[0] = c.u - b.u;
	B[1] = c.v - b.v;

	angle = acos((A[0] * B[0] + A[1] * B[1]) / (d1 * d2)) * (180/PI);

	return angle;
}

//Function to define the values of the identity member of each line (from line fitting algorithm)
void set_line_identity(std::vector<navt::point*> ok_points, navt::line &line){

	navt::point *backPoint, *frontPoint;

	backPoint = ok_points.back();
	frontPoint = ok_points.front();

	line.identity.centroid[0] = (backPoint->u + frontPoint->u)/2;
	line.identity.centroid[1] = (backPoint->v + frontPoint->v)/2;

	line.identity.laser_ids[0] = backPoint->angle_d;
	line.identity.laser_ids[1] = frontPoint->angle_d;

	line.identity.distance = sqrt(line.statistics.lambda_1);
	line.identity.line_roughness = sqrt(line.statistics.lambda_2);

	line.identity.qty_of_points = ok_points.size();

	double mean = ok_points[0]->intensity , var = 0;

	for(int i=0; i<ok_points.size(); i++){
		line.identity.point_indexes.push_back(ok_points[i]->id);
	}
}

//Function to set the properties of the points that belong to the line
void set_point_data(std::vector<navt::point*> ok_points, navt::line &line){
	navt::point point_data;

	double mean = ok_points[0]->intensity , var = 0;

		for(int i=0; i<ok_points.size(); i++){
			point_data.intensity = ok_points[i]->intensity;
			point_data.id = ok_points[i]->id;
			point_data.distance = ok_points[i]->distance;
			point_data.angle_r = ok_points[i]->angle_r;
			point_data.angle_d = ok_points[i]->angle_d;
			point_data.x = ok_points[i]->x;
			point_data.y = ok_points[i]->y;
			point_data.z = ok_points[i]->z;
			point_data.u = ok_points[i]->u;
			point_data.v = ok_points[i]->v;
			line.points.push_back(point_data);

			if(i != 0){
				var = (i-1)/i * var + (1/i+1)*pow((ok_points[i]->intensity - mean), 2);
				mean = (i * mean + ok_points[i]->intensity) / (i+1);
			}

			line.identity.intensity[0] = mean;
			line.identity.intensity[1] = var;
		}
}

//Function to calculate the base statistics used for the line fitting algorithm
void line_statistics(std::vector<navt::point*> ok_points, navt::line &line){

	line.statistics.sx = 0;
	line.statistics.sy = 0;
	line.statistics.sxx = 0;
	line.statistics.syy = 0;
	line.statistics.sxy = 0;

	for (int i=0; i<ok_points.size(); i++){
		line.statistics.sx += ok_points[i]->u;
		line.statistics.sy += ok_points[i]->v;
		line.statistics.sxx += ok_points[i]->u * ok_points[i]->u;
		line.statistics.syy += ok_points[i]->v * ok_points[i]->v;
		line.statistics.sxy += ok_points[i]->u * ok_points[i]->v;
	}

	line.statistics.avr_x = line.statistics.sx/ok_points.size();
	line.statistics.avr_y = line.statistics.sy/ok_points.size();

	line.statistics.SSx = line.statistics.sxx - line.statistics.avr_x * line.statistics.sx;
	line.statistics.SSy = line.statistics.syy - line.statistics.avr_y * line.statistics.sy;
	line.statistics.SSxy = line.statistics.sxy - line.statistics.avr_y * line.statistics.sx;

	double rad = sqrt(0.5*(line.statistics.SSx - line.statistics.SSy) * 0.5*(line.statistics.SSx - line.statistics.SSy)
		+ line.statistics.SSxy * line.statistics.SSxy);

	line.statistics.lambda_1 = 0.5*(line.statistics.SSx + line.statistics.SSy) + rad;
	line.statistics.lambda_2 = 0.5*(line.statistics.SSx + line.statistics.SSy) - rad;

  //V1
	line.statistics.eigVec1[0] = line.statistics.SSxy/(line.statistics.lambda_1 - line.statistics.SSx);
	line.statistics.eigVec1[1] = 1;
  //V2
	line.statistics.eigVec2[0] = line.statistics.SSxy/(line.statistics.lambda_2 - line.statistics.SSx);
	line.statistics.eigVec2[1] = 1;
  //V3
	line.statistics.eigVec3[0] = 1;
	line.statistics.eigVec3[1] = line.statistics.SSxy/(line.statistics.lambda_1 - line.statistics.SSy);
  //V4
	line.statistics.eigVec4[0] = 1;
	line.statistics.eigVec4[1] = line.statistics.SSxy/(line.statistics.lambda_2 - line.statistics.SSy);

	line.statistics.theta1 = atan(line.statistics.eigVec1[1]/line.statistics.eigVec1[0]);
	line.statistics.theta2 = atan(line.statistics.eigVec2[1]/line.statistics.eigVec2[0]);

	line.statistics.rho1 = line.statistics.avr_x * cos(line.statistics.theta1) + line.statistics.avr_y * sin(line.statistics.theta1);
	line.statistics.rho2 = line.statistics.avr_x * cos(line.statistics.theta2) + line.statistics.avr_y * sin(line.statistics.theta2);
}

line_fit& line_fit::getScanLines(navt::scan scan){
	int id = 1;
	std::vector<navt::point*> ok_points;
	bool seed = false;

	std::vector<std::vector<navt::point*> > ok_lines;
	navt::point *newPoint, *oldPoint;

	navt::line line;

	line.identity.azimuth_step = scan.azimuth;
	line.identity.numof_scan = numof_scan;

	double d1, d2;
	double angle, distAB, distBC;

	do{
		while (!seed && id < scan.points.size()-3){
			if(scan.points[id].u > 0 && scan.points[id+1].u > 0 && scan.points[id+2].u > 0){
				distAB = dist_btw_points(scan.points[id],scan.points[id+1]);
				distBC = dist_btw_points(scan.points[id+1],scan.points[id+2]);

				if (distAB > 0 && distBC > 0){
					angle = fabs(angle_btw_vectors(scan.points[id],scan.points[id+1],scan.points[id+2]));

					if (angle >= 160 && angle <= 200 && distAB < 0.64 && distBC < 0.64){
						ok_points.push_back(&scan.points[id]);
						ok_points.push_back(&scan.points[id+1]);
						ok_points.push_back(&scan.points[id+2]);
						seed = true;
						line_statistics(ok_points, line);
					}
					else { id++; }
				}
				else { id++; }
			}
			else { id++; }
		}

		while (seed && id < scan.points.size()-1){ //seed was added

			oldPoint = ok_points.back();
			newPoint = oldPoint+1;

			d1 = dist_btw_points(*oldPoint, *newPoint);
			d2 = newPoint->u * cos(line.statistics.theta2)
			+ newPoint->v * sin(line.statistics.theta2) - line.statistics.rho2;

			if(d1 <= 0.64 && fabs(d2) <= 0.054){
				ok_points.push_back(newPoint);
				line_statistics(ok_points, line);
				// printf("sx: %f\n", line.statistics.sx);
				// printf("sy: %f\n", line.statistics.sy);
				// printf("sxx: %f\n", line.statistics.sxx);
				// printf("syy: %f\n", line.statistics.syy);
				// printf("sxy: %f\n", line.statistics.sxy);
				// printf("avr_x: %f\n", line.statistics.avr_x);
				// printf("avr_y: %f\n", line.statistics.avr_y);
				// printf("SSx: %f\n", line.statistics.SSx);
				// printf("SSy: %f\n", line.statistics.SSy);
				// printf("SSxy: %f\n", line.statistics.SSxy);
				// printf("lambda_1: %f\n", line.statistics.lambda_1);
				// printf("lambda_2: %f\n", line.statistics.lambda_2);
				// printf("theta_1: %f\n", line.statistics.theta1 * 180/3.14159);
				// printf("theta_2: %f\n", line.statistics.theta2 * 180/3.14159);
				// printf("rho_1: %f\n", line.statistics.rho1);
				// printf("rho_2: %f\n", line.statistics.rho2);
				id = newPoint->id;
			}
			else {									//Cut Line condition 1
				seed = false;
				//function to fill line.identity
				set_line_identity(ok_points,line);
				//function to fill line.points
				set_point_data(ok_points,line);

				azm.line.push_back(line);
				line.points.clear();
				ok_lines.push_back(ok_points);

				id = ok_points.back()->id;
				ok_points.clear();
			}
		}

		if(!seed && id >= scan.points.size()-3) { break; }

		if(seed && id == scan.points.size()-1){  //Cut Line condition 2
			id++;
			//function to fill line.identity
			set_line_identity(ok_points,line);
			//function to fill line.points
			set_point_data(ok_points,line);

			azm.line.push_back(line);
			line.points.clear();
			ok_lines.push_back(ok_points);

			ok_points.clear();
			seed = false;
		}

	} while(id < scan.points.size()-1);
	line_set.azm.push_back(azm);
	azm.line.clear();
	return *this;
}


line_fit& line_fit::getLineStock(navt::scan_pkt &scan_pkt){
	line_set.azm.clear();
	scan_pkt_xyz.scans.clear();
	intensitiesId.intensities.clear();
	navt::scan scan;

	navt::point pointLaser;
	for(int i = 0; i < scan_pkt.scans.size(); i++){
		numof_scan = i;
		scan = scan_pkt.scans[i];

		sort_angle(scan);
		spherical2cartesian(scan);
		UV_coord(scan);
		/*for(int j=0;j<scan.points.size();j++){
			pointLaser=scan.points[j];
			if (pointLaser.angle_d == 0){ //searching for the id 15 laser
				intensitiesId.intensities.push_back(pointLaser);

				std::cout << "intensities " << pointLaser.intensity<< std::endl;

			}
		}*/
		scan_pkt_xyz.scans.push_back(scan);
		/*std::copy(std::begin(scan.points),std::end(scan.points),scanTemp);
		for(int i=0;i<scan.points.size();i++){
			if(scan.points[i].angle_d!=0){
				//scan.points.remove(scan.points[i]);
				std::remove(std::begin(scanTemp),std::end(scanTemp),scanTemp[i]);

			}
		}*/
		line_fit::getScanLines(scan);

	}
	return *this;
}
