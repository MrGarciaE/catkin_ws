#include <navt/read_file.h>

#define PI 3.14159

read_file& read_file::read(std::string filename) {
  fpath = "./src/navt/files/" + filename;

  data.open(fpath.c_str());
  if (data.is_open()){
    std::cout << "File is open" << std::endl;
    int azimuth = 0;

    while(data.good()){
      std::getline (data, vert_str, ',');
      vert_num = atof (vert_str.c_str());
      //std::cout << "Verticals: " << vert_num << std::endl;
      std::getline (data, azm_str, ',');
      azm_num = atof (azm_str.c_str());
      //std::cout << "Azimuth: " << azm_num << std::endl;
      std::getline (data, dist_str);
      dist_num = atof (dist_str.c_str());
      //std::cout << "Distance: " << dist_num << std::endl;

      point.angle_d = vert_num * 180/PI;
      point.angle_r = vert_num;
      point.distance = dist_num;

      if (azimuth == 0) {
        scan.azimuth = azm_num;
        azimuth = azm_num;
      }

      if (azm_num == azimuth) {
        scan.points.push_back(point);
      }
      else if (azm_num > azimuth) {
        scan_pkt.scans.push_back(scan);
        scan.points.clear();
        scan.azimuth = azm_num;
        scan.points.push_back(point);
        azimuth = azm_num;
      }

    }
    data.close();
  }
  else std::cout << "Unable to open the file" << std::endl;

  return *this;
}
