#include <ros/ros.h>
#include <navt/read_file.h>
#include <navt/line_fit.h>
#include <navt/land_segmentation.h>
#include <navt/rviz.h>

int main(int argc, char **argv){

	ros::init(argc, argv, "test_node");

  read_file reader;
  reader.read("cetec_2box.csv");

  line_fit line_stk;
  line_stk.getLineStock(reader.scan_pkt);

  land_segmentation segment;
  segment.getRegions(line_stk.line_set);

  rviz map3D;
  map3D.init();

  ros::Rate r(1);

	while (ros::ok()){
    line_stk.getLineStock(reader.scan_pkt);
    segment.getRegions(line_stk.line_set);
    map3D.publishPoints(line_stk.scan_pkt_xyz);
    map3D.publishLines(line_stk.line_set);
    map3D.publishRegions(segment.closedRegions);
    r.sleep();
		ros::spinOnce();
	}

	return 0;
}
