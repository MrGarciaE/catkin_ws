
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>
#include <geometry_msgs/Twist.h>

class DrRobotMovement
{
private:
    geometry_msgs::Twist cmdVelocity;
    ros::NodeHandle n_;
    ros::Publisher pub_;

public:
    DrRobotMovement()
    {
        pub_ = n_.advertise<geometry_msgs::Twist>("drrobot_cmd_vel_001", 2);
    }
    void stopRobot()
    {
        cmdVelocity.linear.x = 0.0;
        cmdVelocity.angular.z = 0.0;
        pub_.publish(cmdVelocity);
    }
    void moveForward(double Distance, double Velocity)
    {
        int deltaTime = Distance/Velocity;
        if (deltaTime > 0)
        {
        cmdVelocity.linear.x = (-Velocity)*1.05;
        cmdVelocity.angular.z = Velocity;

        pub_.publish(cmdVelocity);
        sleep(deltaTime);
        stopRobot();
        }
    }
    void moveBackwards (double Distance, double Velocity)
    {
        int deltaTime = Distance/Velocity;
        if (deltaTime > 0)
        {
        cmdVelocity.linear.x = Velocity;
        cmdVelocity.angular.z = -Velocity;


        pub_.publish(cmdVelocity);
        sleep(deltaTime);
        stopRobot();
        }
    }
    void TurnRight (double TurnOpenning, double Velocity)
    {
        double const WheelsSepparation = 0.5500;
        double const PI = 3.141592654;

        double deltaWheelsMovement = (TurnOpenning/360)*(2*PI*WheelsSepparation);
        double deltaTime = deltaWheelsMovement/0.50;

        if (deltaTime > 0)
        {
        cmdVelocity.linear.x = -Velocity;
        cmdVelocity.angular.z = -Velocity;

        pub_.publish(cmdVelocity);
        sleep(deltaTime);
        stopRobot();
        }
    }
    void TurnLeft (double TurnOpenning, double Velocity)
    {
        double const WheelsSepparation = 0.5500;
        double const PI = 3.141592654;

        double deltaWheelsMovement = (TurnOpenning/360)*(2*PI*WheelsSepparation);
        double deltaTime = deltaWheelsMovement/0.40;

        if (deltaTime > 0)
        {
        cmdVelocity.linear.x = Velocity-0.05;
        cmdVelocity.angular.z = Velocity-0.05;

        pub_.publish(cmdVelocity);
        sleep(1.1);
        stopRobot();
        }
    }
};

int main(int argc, char** argv)
{
    using std::cout;
    using std::cin;
    using std::endl;

    std::string TypeOfDisp;
    std::string Choose;

    double DispDistance;
    double DispSpeed;

    int numberOfMoves;
        numberOfMoves = 5;
    double Route[numberOfMoves][4];

    cout << "Choose: run Demo or capture Custom route (D/C)" << endl;
    cin >> Choose;

    if ((Choose == "D")|(Choose == "d"))
    {

        //Initial move.
        Route[0][0]= 0;
        Route[0][1]= 1;
        Route[0][2]= 0.5;
        Route[0][3]= 1;
        //First Move: Forward 3.5 m @ 0.4 m/s
        Route[1][0]= 0;
        Route[1][1]= 2;
        Route[1][2]= 0.5;
        Route[1][3]= 1;
        //2nd Move: Right Turn 90°
        Route[2][0]= 2;
        Route[2][1]= 90;
        Route[2][2]= 0.5;
        Route[2][3]= 1;
        //3rd Move: Forward 2 m @ 0.4 m/s
        Route[3][0]= 0;
        Route[3][1]= 2;
        Route[3][2]= 0.5;
        Route[3][3]= 1;
        //4th Move: Left turn 90°
        Route[4][0]= 3;
        Route[4][1]= 90;
        Route[4][2]= 0.5;
        Route[4][3]= 0;
/*        //5th Move: Forward 1 m @ 0.4 m/s
        Route[5][0]= 0;
        Route[5][1]= 9;
        Route[5][2]= 0.3;
        Route[5][3]= 1;
        //6th Move: Left Turn 90°
        Route[6][0]= 2;
        Route[6][1]= 90;
        Route[6][2]= 0.5;
        Route[6][3]= 1;
        //7th Move: Forward 6 m @ 0.4 m/s
        Route[7][0]= 0;
        Route[7][1]= 13.2;
        Route[7][2]= 0.3;
        Route[7][3]= 1;
        //8th Move: Left turn 90°
        Route[8][0]= 3;
        Route[8][1]= 90;
        Route[8][2]= 0.5;
        Route[8][3]= 1;
        //7th Move: Forward 4 m @ 0.4 m/s
        Route[9][0]= 0;
        Route[9][1]= 0.5;
        Route[9][2]= 0.3;
        Route[9][3]= 0;
*/

/*
        numberOfMoves = 0;
        cout << "Number of moves required" << endl;
        cin >> numberOfMoves;
        double Route[numberOfMoves][4];
        for (int i = 0; i < numberOfMoves; i ++)
        {
            CaptureTypeOfMove:
            cout << "Movement number " << i+1 << endl;
            cout << "Please indicate type of movement (F/B/RT/LT)" << endl;
            cin >> TypeOfDisp;
            if ((TypeOfDisp == "F")|(TypeOfDisp == "f"))
            {
                Route[i][0]=0;
            }
            else if ((TypeOfDisp =="B")|(TypeOfDisp =="b"))
            {
                Route[i][0]=1;
            }
            else if ((TypeOfDisp == "RT")|(TypeOfDisp == "rt"))
            {
                Route[i][0]=2;
            }
            else if ((TypeOfDisp == "LT")|(TypeOfDisp == "lt"))
            {
                Route[i][0]=3;
            }
            else
            {
                cout << "No valid movement, please try again";
                goto CaptureTypeOfMove;
            }

            cout << "Distance to displace / Turn opening angle" << endl;
            cin >> DispDistance;
            Route[i][1] = DispDistance;

            cout << "Displacement speed" << endl;
            cin >> DispSpeed;
            Route[i][2] = DispSpeed;

            if (i < (numberOfMoves-1))
            {
                Route[i][3]=1;
            }
            else
            {
                Route[i][3]=0;
            }
        }*/
    }

    ros::init(argc, argv, "jaguar_navigation_node1", ros::init_options::AnonymousName | ros::init_options::NoSigintHandler);
    DrRobotMovement RoutePath;

    for (int i = 0; i < numberOfMoves; i ++)
    {
        double d = Route[i][1];
        double v = Route[i][2];

        cout << "Press Enter to continue" << endl;
        cin.get();

        if (Route[i][0] == 0)
        {
            RoutePath.moveForward(d,v);
            cout << "Forward " << d << " m  @ " << v << "m/s" <<endl;
        }
        else if (Route[i][0] == 1)
        {
            RoutePath.moveBackwards(d,v);
            cout << "Backward " << d << " m  @ " << v << "m/s" <<endl;
        }
        else if (Route[i][0] == 2)
        {
            RoutePath.TurnRight(d,v);
            cout << "Turning right " << d << " degrees at " << v << "m/s" <<endl;
        }
        else if (Route[i][0] == 3)
        {
            RoutePath.TurnLeft(d,v);
            cout << "Turning left " << d << " degrees at " << v << "m/s" <<endl;
        }
        if(Route[i][3] == 0)
        {
            break;
        }
    }
    ros::shutdown();
}


