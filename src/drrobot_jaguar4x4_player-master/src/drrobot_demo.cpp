
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>
#include <geometry_msgs/Twist.h>

class DrRobotMovement
{
private:
    geometry_msgs::Twist cmdVelocity;
    ros::NodeHandle n_;
    ros::Publisher pub_;

public:
    DrRobotMovement()
    {
        pub_ = n_.advertise<geometry_msgs::Twist>("drrobot_cmd_vel", 1);
    }
    //-DrRobotMovement() { }

    void stopRobot()
    {
        cmdVelocity.linear.x = 0.0;
        cmdVelocity.angular.z = 0.0;
        pub_.publish(cmdVelocity);
    }

    void moveForward(double Distance, double Velocity)
    {
        double deltaTime = Distance/Velocity;
        if (deltaTime > 0)
        {
        cmdVelocity.linear.x = Velocity;
        cmdVelocity.angular.z = 0;
        std::cout << "Forward " << Distance << " m  @ " << Velocity << "m/s" <<std::endl;

        pub_.publish(cmdVelocity);

        sleep(deltaTime);
        stopRobot();
        }
    }
    void moveBackwards (double Distance, double Velocity)
    {
        double deltaTime = Distance/Velocity;
        if (deltaTime > 0)
        {
        cmdVelocity.linear.x = -Velocity;
        cmdVelocity.angular.z = 0;
        std::cout << "Backwards " << Distance << " m  @ " << Velocity << "m/s" <<std::endl;
        pub_.publish(cmdVelocity);
        sleep(deltaTime);
        stopRobot();
        }
    }
    void TurnRight (double TurnOpenning, double Velocity)
    {
        double const WheelsSepparation = 0.43;
        double const PI = 3.141592654;

        double deltaWheelsMovement = (TurnOpenning/360)*(2*PI*WheelsSepparation);
        double deltaTime = deltaWheelsMovement/Velocity;

        if (deltaTime > 0)
        {
        cmdVelocity.linear.x = 0;
        cmdVelocity.angular.z = -Velocity;
        std::cout << "Turning right " << TurnOpenning << " degrees at " << Velocity << "m/s" <<std::endl;
        pub_.publish(cmdVelocity);
        sleep(deltaTime);
        stopRobot();
        }
    }
    void TurnLeft (double TurnOpenning, double Velocity)
    {
        double const WheelsSepparation = 0.43;
        double const PI = 3.141592654;

        double deltaWheelsMovement = (TurnOpenning/360)*(2*PI*WheelsSepparation);
        double deltaTime = deltaWheelsMovement/Velocity;

        if (deltaTime > 0)
        {
        cmdVelocity.linear.x = 0;
        cmdVelocity.angular.z = Velocity;
        std::cout << "Turning left " << TurnOpenning << " degrees at " << Velocity << "m/s" <<std::endl;
        pub_.publish(cmdVelocity);
        sleep(deltaTime);
        stopRobot();
        }
    }
};

int main(int argc, char** argv)
{
    using std::cout;
    using std::cin;
    using std::endl;

    int numberOfMoves;
    numberOfMoves = 5;

    double Route[numberOfMoves][4];
    // Route[i][0] = 0: Forwards movement
    // Route[i][0] = 1: Backwards movement
    // Route[i][0] = 2: Right Turn
    // Route[i][0] = 3: Left Turn
    // Route[i][1] = n: Number of meters to advance or degrees to turn.
    // Route[i][2] = 0.5: Speed at 0.5 m/s
    // Route[i][3] = 1 / 0;

    //Initial move: Waste move.
//    Route[0][0]= 0;
//    Route[0][1]= 1;
//    Route[0][2]= 0.5;
//    Route[0][3]= 1;
    //First Move: Forward 1 m @ 0.5 m/s
    Route[0][0]= 0;
    Route[0][1]= 0.8;
    Route[0][2]= 0.4;
    Route[0][3]= 1;
    //2nd Move: Left Turn 90°
    Route[1][0]= 3;
    Route[1][1]= 90;
    Route[1][2]= 0.4;
    Route[1][3]= 1;
    //3rd Move: Forward 2 m @ 0.5 m/s
    Route[2][0]= 0;
    Route[2][1]= 0.8;
    Route[2][2]= 0.4;
    Route[2][3]= 1;
    //4th Move: Left turn 90°
    Route[3][0]= 3;
    Route[3][1]= 90;
    Route[3][2]= 0.4;
    Route[3][3]= 1;
    //5th Move: Forward 1 m @ 0.5 m/s
    Route[4][0]= 0;
    Route[4][1]= 0.8;
    Route[4][2]= 0.4;
    Route[4][3]= 0;

    ros::init(argc, argv, "drrobot_jaguar4x4_demo_node", ros::init_options::AnonymousName | ros::init_options::NoSigintHandler);

    DrRobotMovement RoutePath;

    for (int i = 0; i < numberOfMoves; i ++)
    {
        double d = Route[i][1];
        double v = Route[i][2];

        cout << "Press Enter to continue" << endl;
        cin.get();

        if (Route[i][0] == 0)
        {
            RoutePath.moveForward(d,v);
        }
        else if (Route[i][0] == 1)
        {
            RoutePath.moveBackwards(d,v);
        }
        else if (Route[i][0] == 2)
        {
            RoutePath.TurnRight(d,v);
        }
        else if (Route[i][0] == 3)
        {
            RoutePath.TurnLeft(d,v);
        }
        if(Route[i][3] == 0)
        {
            break;
        }
    }
    ros::shutdown();
}
