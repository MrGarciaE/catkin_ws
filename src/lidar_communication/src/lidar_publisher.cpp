#include <string>
#include <iostream>
#include <pcap.h>
#include "math.h"
#include <stdint.h>

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "lidar_communication/scan_360.h"
#include "lidar_communication/scan.h"
#include "lidar_communication/point.h"

#include <sstream>

using namespace std;

uint16_t concatenate8BigEndian(uint8_t lsb, uint8_t msb){
  return (uint16_t)(msb << 8) | (uint8_t)lsb;
}

const double vert_angle[32] = {-0.5352924815866609,
  -0.1628392174657417,
  -0.5119050696099369,
  -0.13962634015954636,
  -0.4886921905584123,
  -0.11641346285335104,
  -0.4654793115068877,
  -0.09302604738596851,
  -0.44209189953016365,
  -0.06981317007977318,
  -0.4188790204786391,
  -0.046600292773577856,
  -0.39566614142711454,
  -0.023212879051524585,
  -0.3722787294503905,
  0.0,
  -0.3490658503988659,
  0.023212879051524585,
  -0.32585297134734137,
  0.046600292773577856,
  -0.30246555937061725,
  0.06981317007977318,
  -0.2792526803190927,
  0.09302604738596851,
  -0.25603980126756815,
  0.11641346285335104,
  -0.23265238929084414,
  0.13962634015954636,
  -0.20943951023931956,
  0.1628392174657417,
  -0.18622663118779495,
  0.18622663118779495};


/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
 int main(int argc, char **argv)
 {


  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */

  // string file = "./PATL_newshape02-Data.pcap";
  // char errbuff[PCAP_ERRBUF_SIZE];
  // pcap_t * pcap = pcap_open_offline(file.c_str(), errbuff);
  // struct pcap_pkthdr *header;
  // const u_char *data;

   int block_offset = 0;
   int laser_offset = 0;
   u_int packetCount = 0;

   uint16_t rotation = 0;
   uint8_t arr_intensity[32] = {};
   uint16_t arr_distance[32] = {};
   int intensity_index = 0;
   int distance_index = 0;



  pcap_t *handle;         /* Session handle */
  char *dev;          /* The device to sniff on */
  char errbuf[PCAP_ERRBUF_SIZE];  /* Error string */
  struct bpf_program fp;      /* The compiled filter */
  char filter_exp[] = "port 2368";    /* The filter expression */
  bpf_u_int32 mask;       /* Our netmask */
  bpf_u_int32 net;        /* Our IP */
  struct pcap_pkthdr header;  /* The header that pcap gives us */
  const u_char *data;       /* The actual packet */

  /* Define the device */
  //dev = pcap_lookupdev(errbuf);
   dev = "eth0";
   if (dev == NULL) {
    fprintf(stderr, "Couldn't find default device: %s\n", errbuf);
    return(2);
  }
  /* Find the properties for the device */
  if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1) {
    fprintf(stderr, "Couldn't get netmask for device %s: %s\n", dev, errbuf);
    net = 0;
    mask = 0;
  }
  /* Open the session in promiscuous mode */
  handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
  if (handle == NULL) {
    fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
    return(2);
  }

      /* Compile and apply the filter */
  if (pcap_compile(handle, &fp, filter_exp, 0, net) == -1) {
    fprintf(stderr, "Couldn't parse filter %s: %s\n", filter_exp, pcap_geterr(handle));
    return(2);
  }
  if (pcap_setfilter(handle, &fp) == -1) {
    fprintf(stderr, "Couldn't install filter %s: %s\n", filter_exp, pcap_geterr(handle));
    return(2);
  }




  ros::init(argc, argv, "talker");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
   ros::NodeHandle n;

  /**
   * The advertise() function is how you tell ROS that you want to
   * publish on a given topic name. This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing. After this advertise() call is made, the master
   * node will notify anyone who is trying to subscribe to this topic name,
   * and they will in turn negotiate a peer-to-peer connection with this
   * node.  advertise() returns a Publisher object which allows you to
   * publish messages on that topic through a call to publish().  Once
   * all copies of the returned Publisher object are destroyed, the topic
   * will be automatically unadvertised.
   *
   * The second parameter to advertise() is the size of the message queue
   * used for publishing messages.  If messages are published more quickly
   * than we can send them, the number here specifies how many messages to
   * buffer up before throwing some away.
   */
   ros::Publisher lidar_publisher = n.advertise<lidar_communication::scan_360>("lidar_publisher", 1);
   lidar_communication::scan_360 scan_360;
   lidar_communication::scan scan;
   lidar_communication::point point;
   int azimuth = 0;

  //ros::Rate loop_rate(1000);

  /**
   * A count of how many messages we have sent. This is used to create
   * a unique string for each message.
   */
   int count = 0;
   while (ros::ok()){
    while (1){
      data = pcap_next(handle, &header);
      // Print using printf. See printf reference:
      // http://www.cplusplus.com/reference/clibrary/cstdio/printf/

      // Show the packet number
      // printf("Packet # %i\n", ++packetCount);

      // // Show the size in bytes of the packet
      // printf("Packet size: %ld bytes\n", (long int)header->len);

      // // Show a warning if the length captured is different
      // if (header->len != header->caplen)
      //     printf("Warning! Capture size different than packet size: %ld bytes\n", (long int)header->len);

      // // Show Epoch Time
      // printf("Epoch Time: %ld:%ld seconds\n", header->ts.tv_sec, header->ts.tv_usec);

      for (int block=0; block<12; block++){
        block_offset = (block*100)+42; //42 to ignore the ethernet header
        laser_offset = 4;

        rotation = concatenate8BigEndian(data[block_offset+2], data[block_offset+3]);
        scan.azimuth = rotation;
        //printf("%i\t", scan.azimuth);

        for (int laser=0; laser<32; laser++){
          distance_index = block_offset+laser_offset;
          intensity_index = block_offset+laser_offset + 2;

          point.intensity = data[intensity_index];
          point.distance = concatenate8BigEndian(data[distance_index], data[distance_index+1]);
          point.vertical_angle = vert_angle[laser];

          //ROS_INFO("intensity %i", point.intensity);

          // arr_distance[laser] = concatenate8BigEndian(data[distance_index], data[distance_index+1]);
          // arr_intensity[laser] = data[intensity_index];
          scan.points.push_back(point);
          laser_offset += 3; //next 3 bytes (2 distance, 1 intensity)

        }

        scan_360.scans.push_back(scan);
        scan.points.clear();
      }

      if (scan.azimuth<azimuth){
        lidar_publisher.publish(scan_360);
        // printf("%li\n", scan_360.scans.size());
        scan_360.scans.clear();
      }

      azimuth = scan.azimuth;
    }


    //ROS_INFO("Termino de leer archivo");



    /**
     * This is a message object. You stuff it with data, and then publish it.
     */
    // std_msgs::String msg;

    // std::stringstream ss;
    // ss << "hello world " << count;
    // msg.data = ss.str();

    // ROS_INFO("%s", msg.data.c_str());

    /**
     * The publish() function is how you send messages. The parameter
     * is the message object. The type of this object must agree with the type
     * given as a template parameter to the advertise<>() call, as was done
     * in the constructor above.
     */
   }


   return 0;
 }