# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/erobots/catkin_ws/install/include".split(';') if "/home/erobots/catkin_ws/install/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;rospy;std_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lnavt".split(';') if "-lnavt" != "" else []
PROJECT_NAME = "navt"
PROJECT_SPACE_DIR = "/home/erobots/catkin_ws/install"
PROJECT_VERSION = "0.0.0"
