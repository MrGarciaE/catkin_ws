# CMake generated Testfile for 
# Source directory: /home/erobots/catkin_ws/src
# Build directory: /home/erobots/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(DrRobotMotionSensorDriver)
SUBDIRS(beginner_tutorials)
SUBDIRS(jaguar4x4_2014-master)
SUBDIRS(jaguar_navigation)
SUBDIRS(lidar_communication)
SUBDIRS(navt)
SUBDIRS(drrobot_jaguar4x4_player-master)
SUBDIRS(drrobot_jaguarV2_player)
