# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/erobots/catkin_ws/devel/include;/home/erobots/catkin_ws/src/drrobot_jaguarV2_player/include".split(';') if "/home/erobots/catkin_ws/devel/include;/home/erobots/catkin_ws/src/drrobot_jaguarV2_player/include" != "" else []
PROJECT_CATKIN_DEPENDS = "DrRobotMotionSensorDriver;geometry_msgs;nav_msgs;roscpp;roslib;rospy;sensor_msgs;std_msgs;tf".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "drrobot_jaguarV2_player"
PROJECT_SPACE_DIR = "/home/erobots/catkin_ws/devel"
PROJECT_VERSION = "1.2.1"
