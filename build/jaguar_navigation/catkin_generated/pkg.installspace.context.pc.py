# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/erobots/catkin_ws/install/include".split(';') if "/home/erobots/catkin_ws/install/include" != "" else []
PROJECT_CATKIN_DEPENDS = "actionlib_msgs;geometry_msgs;roscpp;rospy;std_msgs;message_generation".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "jaguar_navigation"
PROJECT_SPACE_DIR = "/home/erobots/catkin_ws/install"
PROJECT_VERSION = "0.0.0"
