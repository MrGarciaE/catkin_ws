# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "jaguar_navigation: 1 messages, 0 services")

set(MSG_I_FLAGS "-Ijaguar_navigation:/home/erobots/catkin_ws/src/jaguar_navigation/msg;-Igeometry_msgs:/opt/ros/indigo/share/geometry_msgs/cmake/../msg;-Istd_msgs:/opt/ros/indigo/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(genlisp REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(jaguar_navigation_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/erobots/catkin_ws/src/jaguar_navigation/msg/Num.msg" NAME_WE)
add_custom_target(_jaguar_navigation_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "jaguar_navigation" "/home/erobots/catkin_ws/src/jaguar_navigation/msg/Num.msg" ""
)

#
#  langs = gencpp;genlisp;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(jaguar_navigation
  "/home/erobots/catkin_ws/src/jaguar_navigation/msg/Num.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/jaguar_navigation
)

### Generating Services

### Generating Module File
_generate_module_cpp(jaguar_navigation
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/jaguar_navigation
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(jaguar_navigation_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(jaguar_navigation_generate_messages jaguar_navigation_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/erobots/catkin_ws/src/jaguar_navigation/msg/Num.msg" NAME_WE)
add_dependencies(jaguar_navigation_generate_messages_cpp _jaguar_navigation_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(jaguar_navigation_gencpp)
add_dependencies(jaguar_navigation_gencpp jaguar_navigation_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS jaguar_navigation_generate_messages_cpp)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(jaguar_navigation
  "/home/erobots/catkin_ws/src/jaguar_navigation/msg/Num.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/jaguar_navigation
)

### Generating Services

### Generating Module File
_generate_module_lisp(jaguar_navigation
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/jaguar_navigation
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(jaguar_navigation_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(jaguar_navigation_generate_messages jaguar_navigation_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/erobots/catkin_ws/src/jaguar_navigation/msg/Num.msg" NAME_WE)
add_dependencies(jaguar_navigation_generate_messages_lisp _jaguar_navigation_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(jaguar_navigation_genlisp)
add_dependencies(jaguar_navigation_genlisp jaguar_navigation_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS jaguar_navigation_generate_messages_lisp)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(jaguar_navigation
  "/home/erobots/catkin_ws/src/jaguar_navigation/msg/Num.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/jaguar_navigation
)

### Generating Services

### Generating Module File
_generate_module_py(jaguar_navigation
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/jaguar_navigation
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(jaguar_navigation_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(jaguar_navigation_generate_messages jaguar_navigation_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/erobots/catkin_ws/src/jaguar_navigation/msg/Num.msg" NAME_WE)
add_dependencies(jaguar_navigation_generate_messages_py _jaguar_navigation_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(jaguar_navigation_genpy)
add_dependencies(jaguar_navigation_genpy jaguar_navigation_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS jaguar_navigation_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/jaguar_navigation)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/jaguar_navigation
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
add_dependencies(jaguar_navigation_generate_messages_cpp geometry_msgs_generate_messages_cpp)
add_dependencies(jaguar_navigation_generate_messages_cpp std_msgs_generate_messages_cpp)

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/jaguar_navigation)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/jaguar_navigation
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
add_dependencies(jaguar_navigation_generate_messages_lisp geometry_msgs_generate_messages_lisp)
add_dependencies(jaguar_navigation_generate_messages_lisp std_msgs_generate_messages_lisp)

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/jaguar_navigation)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/jaguar_navigation\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/jaguar_navigation
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
add_dependencies(jaguar_navigation_generate_messages_py geometry_msgs_generate_messages_py)
add_dependencies(jaguar_navigation_generate_messages_py std_msgs_generate_messages_py)
