# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/erobots/catkin_ws/devel/include;/home/erobots/catkin_ws/src/drrobot_jaguar4x4_player-master/include".split(';') if "/home/erobots/catkin_ws/devel/include;/home/erobots/catkin_ws/src/drrobot_jaguar4x4_player-master/include" != "" else []
PROJECT_CATKIN_DEPENDS = "DrRobotMotionSensorDriver;geometery_msgs;nav_msgs;roscpp;roslib;rospy;sensor_msgs;std_msgs;tf;message_runtime".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "drrobot_jaguar4x4_player"
PROJECT_SPACE_DIR = "/home/erobots/catkin_ws/devel"
PROJECT_VERSION = "1.2.1"
