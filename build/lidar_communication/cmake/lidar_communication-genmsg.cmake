# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "lidar_communication: 3 messages, 0 services")

set(MSG_I_FLAGS "-Ilidar_communication:/home/erobots/catkin_ws/src/lidar_communication/msg;-Istd_msgs:/opt/ros/indigo/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(genlisp REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(lidar_communication_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/erobots/catkin_ws/src/lidar_communication/msg/scan.msg" NAME_WE)
add_custom_target(_lidar_communication_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "lidar_communication" "/home/erobots/catkin_ws/src/lidar_communication/msg/scan.msg" "lidar_communication/point"
)

get_filename_component(_filename "/home/erobots/catkin_ws/src/lidar_communication/msg/point.msg" NAME_WE)
add_custom_target(_lidar_communication_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "lidar_communication" "/home/erobots/catkin_ws/src/lidar_communication/msg/point.msg" ""
)

get_filename_component(_filename "/home/erobots/catkin_ws/src/lidar_communication/msg/scan_360.msg" NAME_WE)
add_custom_target(_lidar_communication_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "lidar_communication" "/home/erobots/catkin_ws/src/lidar_communication/msg/scan_360.msg" "lidar_communication/point:lidar_communication/scan"
)

#
#  langs = gencpp;genlisp;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(lidar_communication
  "/home/erobots/catkin_ws/src/lidar_communication/msg/scan.msg"
  "${MSG_I_FLAGS}"
  "/home/erobots/catkin_ws/src/lidar_communication/msg/point.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/lidar_communication
)
_generate_msg_cpp(lidar_communication
  "/home/erobots/catkin_ws/src/lidar_communication/msg/point.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/lidar_communication
)
_generate_msg_cpp(lidar_communication
  "/home/erobots/catkin_ws/src/lidar_communication/msg/scan_360.msg"
  "${MSG_I_FLAGS}"
  "/home/erobots/catkin_ws/src/lidar_communication/msg/point.msg;/home/erobots/catkin_ws/src/lidar_communication/msg/scan.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/lidar_communication
)

### Generating Services

### Generating Module File
_generate_module_cpp(lidar_communication
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/lidar_communication
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(lidar_communication_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(lidar_communication_generate_messages lidar_communication_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/erobots/catkin_ws/src/lidar_communication/msg/scan.msg" NAME_WE)
add_dependencies(lidar_communication_generate_messages_cpp _lidar_communication_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/erobots/catkin_ws/src/lidar_communication/msg/point.msg" NAME_WE)
add_dependencies(lidar_communication_generate_messages_cpp _lidar_communication_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/erobots/catkin_ws/src/lidar_communication/msg/scan_360.msg" NAME_WE)
add_dependencies(lidar_communication_generate_messages_cpp _lidar_communication_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(lidar_communication_gencpp)
add_dependencies(lidar_communication_gencpp lidar_communication_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS lidar_communication_generate_messages_cpp)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(lidar_communication
  "/home/erobots/catkin_ws/src/lidar_communication/msg/scan.msg"
  "${MSG_I_FLAGS}"
  "/home/erobots/catkin_ws/src/lidar_communication/msg/point.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/lidar_communication
)
_generate_msg_lisp(lidar_communication
  "/home/erobots/catkin_ws/src/lidar_communication/msg/point.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/lidar_communication
)
_generate_msg_lisp(lidar_communication
  "/home/erobots/catkin_ws/src/lidar_communication/msg/scan_360.msg"
  "${MSG_I_FLAGS}"
  "/home/erobots/catkin_ws/src/lidar_communication/msg/point.msg;/home/erobots/catkin_ws/src/lidar_communication/msg/scan.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/lidar_communication
)

### Generating Services

### Generating Module File
_generate_module_lisp(lidar_communication
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/lidar_communication
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(lidar_communication_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(lidar_communication_generate_messages lidar_communication_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/erobots/catkin_ws/src/lidar_communication/msg/scan.msg" NAME_WE)
add_dependencies(lidar_communication_generate_messages_lisp _lidar_communication_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/erobots/catkin_ws/src/lidar_communication/msg/point.msg" NAME_WE)
add_dependencies(lidar_communication_generate_messages_lisp _lidar_communication_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/erobots/catkin_ws/src/lidar_communication/msg/scan_360.msg" NAME_WE)
add_dependencies(lidar_communication_generate_messages_lisp _lidar_communication_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(lidar_communication_genlisp)
add_dependencies(lidar_communication_genlisp lidar_communication_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS lidar_communication_generate_messages_lisp)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(lidar_communication
  "/home/erobots/catkin_ws/src/lidar_communication/msg/scan.msg"
  "${MSG_I_FLAGS}"
  "/home/erobots/catkin_ws/src/lidar_communication/msg/point.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/lidar_communication
)
_generate_msg_py(lidar_communication
  "/home/erobots/catkin_ws/src/lidar_communication/msg/point.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/lidar_communication
)
_generate_msg_py(lidar_communication
  "/home/erobots/catkin_ws/src/lidar_communication/msg/scan_360.msg"
  "${MSG_I_FLAGS}"
  "/home/erobots/catkin_ws/src/lidar_communication/msg/point.msg;/home/erobots/catkin_ws/src/lidar_communication/msg/scan.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/lidar_communication
)

### Generating Services

### Generating Module File
_generate_module_py(lidar_communication
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/lidar_communication
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(lidar_communication_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(lidar_communication_generate_messages lidar_communication_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/erobots/catkin_ws/src/lidar_communication/msg/scan.msg" NAME_WE)
add_dependencies(lidar_communication_generate_messages_py _lidar_communication_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/erobots/catkin_ws/src/lidar_communication/msg/point.msg" NAME_WE)
add_dependencies(lidar_communication_generate_messages_py _lidar_communication_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/erobots/catkin_ws/src/lidar_communication/msg/scan_360.msg" NAME_WE)
add_dependencies(lidar_communication_generate_messages_py _lidar_communication_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(lidar_communication_genpy)
add_dependencies(lidar_communication_genpy lidar_communication_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS lidar_communication_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/lidar_communication)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/lidar_communication
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
add_dependencies(lidar_communication_generate_messages_cpp std_msgs_generate_messages_cpp)

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/lidar_communication)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/lidar_communication
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
add_dependencies(lidar_communication_generate_messages_lisp std_msgs_generate_messages_lisp)

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/lidar_communication)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/lidar_communication\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/lidar_communication
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
add_dependencies(lidar_communication_generate_messages_py std_msgs_generate_messages_py)
