# This Python file uses the following encoding: utf-8
"""autogenerated by genpy from lidar_communication/point.msg. Do not edit."""
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct


class point(genpy.Message):
  _md5sum = "b41704046e427baa96e5c3bfe4f531d7"
  _type = "lidar_communication/point"
  _has_header = False #flag to mark the presence of a Header object
  _full_text = """uint8 intensity
uint8 id
uint16 distance
float64 vertical_angle
float64 x
float64 y
float64 z
float64 u
float64 v
"""
  __slots__ = ['intensity','id','distance','vertical_angle','x','y','z','u','v']
  _slot_types = ['uint8','uint8','uint16','float64','float64','float64','float64','float64','float64']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       intensity,id,distance,vertical_angle,x,y,z,u,v

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(point, self).__init__(*args, **kwds)
      #message fields cannot be None, assign default values for those that are
      if self.intensity is None:
        self.intensity = 0
      if self.id is None:
        self.id = 0
      if self.distance is None:
        self.distance = 0
      if self.vertical_angle is None:
        self.vertical_angle = 0.
      if self.x is None:
        self.x = 0.
      if self.y is None:
        self.y = 0.
      if self.z is None:
        self.z = 0.
      if self.u is None:
        self.u = 0.
      if self.v is None:
        self.v = 0.
    else:
      self.intensity = 0
      self.id = 0
      self.distance = 0
      self.vertical_angle = 0.
      self.x = 0.
      self.y = 0.
      self.z = 0.
      self.u = 0.
      self.v = 0.

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      _x = self
      buff.write(_struct_2BH6d.pack(_x.intensity, _x.id, _x.distance, _x.vertical_angle, _x.x, _x.y, _x.z, _x.u, _x.v))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(_x))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(_x))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    try:
      end = 0
      _x = self
      start = end
      end += 52
      (_x.intensity, _x.id, _x.distance, _x.vertical_angle, _x.x, _x.y, _x.z, _x.u, _x.v,) = _struct_2BH6d.unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      _x = self
      buff.write(_struct_2BH6d.pack(_x.intensity, _x.id, _x.distance, _x.vertical_angle, _x.x, _x.y, _x.z, _x.u, _x.v))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(_x))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(_x))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    try:
      end = 0
      _x = self
      start = end
      end += 52
      (_x.intensity, _x.id, _x.distance, _x.vertical_angle, _x.x, _x.y, _x.z, _x.u, _x.v,) = _struct_2BH6d.unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill

_struct_I = genpy.struct_I
_struct_2BH6d = struct.Struct("<2BH6d")
