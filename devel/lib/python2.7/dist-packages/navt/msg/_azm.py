# This Python file uses the following encoding: utf-8
"""autogenerated by genpy from navt/azm.msg. Do not edit."""
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct

import navt.msg

class azm(genpy.Message):
  _md5sum = "27047a6870985f12e65973274e3ea9c0"
  _type = "navt/azm"
  _has_header = False #flag to mark the presence of a Header object
  _full_text = """line[] line
================================================================================
MSG: navt/line
statistics statistics
identity identity
point[] points

================================================================================
MSG: navt/statistics
float64 sx
float64 sy
float64 sxx
float64 syy
float64 sxy
float64 avr_x
float64 avr_y
float64 SSx
float64 SSy
float64 SSxy
float64 lambda_1
float64 lambda_2
float64 theta1
float64 theta2
float64 rho1
float64 rho2
float64[2] eigVec1
float64[2] eigVec2
float64[2] eigVec3
float64[2] eigVec4
================================================================================
MSG: navt/identity
float64 distance
float64 line_roughness
uint8 qty_of_points
uint8[] point_indexes
float64[2] centroid
uint16 azimuth_step
float64[2] intensity
uint16 planar_region
uint16 numof_scan
float64[2] laser_ids
uint8[2] frontier_indexes

================================================================================
MSG: navt/point
uint8 intensity
uint8 id
uint16 distance
float64 angle_d
float64 angle_r
float64 x
float64 y
float64 z
float64 u
float64 v
"""
  __slots__ = ['line']
  _slot_types = ['navt/line[]']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       line

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(azm, self).__init__(*args, **kwds)
      #message fields cannot be None, assign default values for those that are
      if self.line is None:
        self.line = []
    else:
      self.line = []

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      length = len(self.line)
      buff.write(_struct_I.pack(length))
      for val1 in self.line:
        _v1 = val1.statistics
        _x = _v1
        buff.write(_struct_16d.pack(_x.sx, _x.sy, _x.sxx, _x.syy, _x.sxy, _x.avr_x, _x.avr_y, _x.SSx, _x.SSy, _x.SSxy, _x.lambda_1, _x.lambda_2, _x.theta1, _x.theta2, _x.rho1, _x.rho2))
        buff.write(_struct_2d.pack(*_v1.eigVec1))
        buff.write(_struct_2d.pack(*_v1.eigVec2))
        buff.write(_struct_2d.pack(*_v1.eigVec3))
        buff.write(_struct_2d.pack(*_v1.eigVec4))
        _v2 = val1.identity
        _x = _v2
        buff.write(_struct_2dB.pack(_x.distance, _x.line_roughness, _x.qty_of_points))
        _x = _v2.point_indexes
        length = len(_x)
        # - if encoded as a list instead, serialize as bytes instead of string
        if type(_x) in [list, tuple]:
          buff.write(struct.pack('<I%sB'%length, length, *_x))
        else:
          buff.write(struct.pack('<I%ss'%length, length, _x))
        buff.write(_struct_2d.pack(*_v2.centroid))
        buff.write(_struct_H.pack(_v2.azimuth_step))
        buff.write(_struct_2d.pack(*_v2.intensity))
        _x = _v2
        buff.write(_struct_2H.pack(_x.planar_region, _x.numof_scan))
        buff.write(_struct_2d.pack(*_v2.laser_ids))
        _x = _v2.frontier_indexes
        # - if encoded as a list instead, serialize as bytes instead of string
        if type(_x) in [list, tuple]:
          buff.write(_struct_2B.pack(*_x))
        else:
          buff.write(_struct_2s.pack(_x))
        length = len(val1.points)
        buff.write(_struct_I.pack(length))
        for val2 in val1.points:
          _x = val2
          buff.write(_struct_2BH7d.pack(_x.intensity, _x.id, _x.distance, _x.angle_d, _x.angle_r, _x.x, _x.y, _x.z, _x.u, _x.v))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(_x))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(_x))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    try:
      if self.line is None:
        self.line = None
      end = 0
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.line = []
      for i in range(0, length):
        val1 = navt.msg.line()
        _v3 = val1.statistics
        _x = _v3
        start = end
        end += 128
        (_x.sx, _x.sy, _x.sxx, _x.syy, _x.sxy, _x.avr_x, _x.avr_y, _x.SSx, _x.SSy, _x.SSxy, _x.lambda_1, _x.lambda_2, _x.theta1, _x.theta2, _x.rho1, _x.rho2,) = _struct_16d.unpack(str[start:end])
        start = end
        end += 16
        _v3.eigVec1 = _struct_2d.unpack(str[start:end])
        start = end
        end += 16
        _v3.eigVec2 = _struct_2d.unpack(str[start:end])
        start = end
        end += 16
        _v3.eigVec3 = _struct_2d.unpack(str[start:end])
        start = end
        end += 16
        _v3.eigVec4 = _struct_2d.unpack(str[start:end])
        _v4 = val1.identity
        _x = _v4
        start = end
        end += 17
        (_x.distance, _x.line_roughness, _x.qty_of_points,) = _struct_2dB.unpack(str[start:end])
        start = end
        end += 4
        (length,) = _struct_I.unpack(str[start:end])
        start = end
        end += length
        _v4.point_indexes = str[start:end]
        start = end
        end += 16
        _v4.centroid = _struct_2d.unpack(str[start:end])
        start = end
        end += 2
        (_v4.azimuth_step,) = _struct_H.unpack(str[start:end])
        start = end
        end += 16
        _v4.intensity = _struct_2d.unpack(str[start:end])
        _x = _v4
        start = end
        end += 4
        (_x.planar_region, _x.numof_scan,) = _struct_2H.unpack(str[start:end])
        start = end
        end += 16
        _v4.laser_ids = _struct_2d.unpack(str[start:end])
        start = end
        end += 2
        _v4.frontier_indexes = str[start:end]
        start = end
        end += 4
        (length,) = _struct_I.unpack(str[start:end])
        val1.points = []
        for i in range(0, length):
          val2 = navt.msg.point()
          _x = val2
          start = end
          end += 60
          (_x.intensity, _x.id, _x.distance, _x.angle_d, _x.angle_r, _x.x, _x.y, _x.z, _x.u, _x.v,) = _struct_2BH7d.unpack(str[start:end])
          val1.points.append(val2)
        self.line.append(val1)
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      length = len(self.line)
      buff.write(_struct_I.pack(length))
      for val1 in self.line:
        _v5 = val1.statistics
        _x = _v5
        buff.write(_struct_16d.pack(_x.sx, _x.sy, _x.sxx, _x.syy, _x.sxy, _x.avr_x, _x.avr_y, _x.SSx, _x.SSy, _x.SSxy, _x.lambda_1, _x.lambda_2, _x.theta1, _x.theta2, _x.rho1, _x.rho2))
        buff.write(_v5.eigVec1.tostring())
        buff.write(_v5.eigVec2.tostring())
        buff.write(_v5.eigVec3.tostring())
        buff.write(_v5.eigVec4.tostring())
        _v6 = val1.identity
        _x = _v6
        buff.write(_struct_2dB.pack(_x.distance, _x.line_roughness, _x.qty_of_points))
        _x = _v6.point_indexes
        length = len(_x)
        # - if encoded as a list instead, serialize as bytes instead of string
        if type(_x) in [list, tuple]:
          buff.write(struct.pack('<I%sB'%length, length, *_x))
        else:
          buff.write(struct.pack('<I%ss'%length, length, _x))
        buff.write(_v6.centroid.tostring())
        buff.write(_struct_H.pack(_v6.azimuth_step))
        buff.write(_v6.intensity.tostring())
        _x = _v6
        buff.write(_struct_2H.pack(_x.planar_region, _x.numof_scan))
        buff.write(_v6.laser_ids.tostring())
        _x = _v6.frontier_indexes
        # - if encoded as a list instead, serialize as bytes instead of string
        if type(_x) in [list, tuple]:
          buff.write(_struct_2B.pack(*_x))
        else:
          buff.write(_struct_2s.pack(_x))
        length = len(val1.points)
        buff.write(_struct_I.pack(length))
        for val2 in val1.points:
          _x = val2
          buff.write(_struct_2BH7d.pack(_x.intensity, _x.id, _x.distance, _x.angle_d, _x.angle_r, _x.x, _x.y, _x.z, _x.u, _x.v))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(_x))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(_x))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    try:
      if self.line is None:
        self.line = None
      end = 0
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.line = []
      for i in range(0, length):
        val1 = navt.msg.line()
        _v7 = val1.statistics
        _x = _v7
        start = end
        end += 128
        (_x.sx, _x.sy, _x.sxx, _x.syy, _x.sxy, _x.avr_x, _x.avr_y, _x.SSx, _x.SSy, _x.SSxy, _x.lambda_1, _x.lambda_2, _x.theta1, _x.theta2, _x.rho1, _x.rho2,) = _struct_16d.unpack(str[start:end])
        start = end
        end += 16
        _v7.eigVec1 = numpy.frombuffer(str[start:end], dtype=numpy.float64, count=2)
        start = end
        end += 16
        _v7.eigVec2 = numpy.frombuffer(str[start:end], dtype=numpy.float64, count=2)
        start = end
        end += 16
        _v7.eigVec3 = numpy.frombuffer(str[start:end], dtype=numpy.float64, count=2)
        start = end
        end += 16
        _v7.eigVec4 = numpy.frombuffer(str[start:end], dtype=numpy.float64, count=2)
        _v8 = val1.identity
        _x = _v8
        start = end
        end += 17
        (_x.distance, _x.line_roughness, _x.qty_of_points,) = _struct_2dB.unpack(str[start:end])
        start = end
        end += 4
        (length,) = _struct_I.unpack(str[start:end])
        start = end
        end += length
        _v8.point_indexes = str[start:end]
        start = end
        end += 16
        _v8.centroid = numpy.frombuffer(str[start:end], dtype=numpy.float64, count=2)
        start = end
        end += 2
        (_v8.azimuth_step,) = _struct_H.unpack(str[start:end])
        start = end
        end += 16
        _v8.intensity = numpy.frombuffer(str[start:end], dtype=numpy.float64, count=2)
        _x = _v8
        start = end
        end += 4
        (_x.planar_region, _x.numof_scan,) = _struct_2H.unpack(str[start:end])
        start = end
        end += 16
        _v8.laser_ids = numpy.frombuffer(str[start:end], dtype=numpy.float64, count=2)
        start = end
        end += 2
        _v8.frontier_indexes = str[start:end]
        start = end
        end += 4
        (length,) = _struct_I.unpack(str[start:end])
        val1.points = []
        for i in range(0, length):
          val2 = navt.msg.point()
          _x = val2
          start = end
          end += 60
          (_x.intensity, _x.id, _x.distance, _x.angle_d, _x.angle_r, _x.x, _x.y, _x.z, _x.u, _x.v,) = _struct_2BH7d.unpack(str[start:end])
          val1.points.append(val2)
        self.line.append(val1)
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill

_struct_I = genpy.struct_I
_struct_2BH7d = struct.Struct("<2BH7d")
_struct_2s = struct.Struct("<2s")
_struct_16d = struct.Struct("<16d")
_struct_H = struct.Struct("<H")
_struct_2d = struct.Struct("<2d")
_struct_2B = struct.Struct("<2B")
_struct_2dB = struct.Struct("<2dB")
_struct_2H = struct.Struct("<2H")
