# This Python file uses the following encoding: utf-8
"""autogenerated by genpy from navt/intensities.msg. Do not edit."""
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct

import navt.msg

class intensities(genpy.Message):
  _md5sum = "86f723fcabace2bb83a99614e18dcfdc"
  _type = "navt/intensities"
  _has_header = False #flag to mark the presence of a Header object
  _full_text = """intensity[] intensities
================================================================================
MSG: navt/intensity
uint8 intensity
"""
  __slots__ = ['intensities']
  _slot_types = ['navt/intensity[]']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       intensities

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(intensities, self).__init__(*args, **kwds)
      #message fields cannot be None, assign default values for those that are
      if self.intensities is None:
        self.intensities = []
    else:
      self.intensities = []

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      length = len(self.intensities)
      buff.write(_struct_I.pack(length))
      for val1 in self.intensities:
        buff.write(_struct_B.pack(val1.intensity))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(_x))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(_x))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    try:
      if self.intensities is None:
        self.intensities = None
      end = 0
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.intensities = []
      for i in range(0, length):
        val1 = navt.msg.intensity()
        start = end
        end += 1
        (val1.intensity,) = _struct_B.unpack(str[start:end])
        self.intensities.append(val1)
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      length = len(self.intensities)
      buff.write(_struct_I.pack(length))
      for val1 in self.intensities:
        buff.write(_struct_B.pack(val1.intensity))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(_x))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(_x))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    try:
      if self.intensities is None:
        self.intensities = None
      end = 0
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.intensities = []
      for i in range(0, length):
        val1 = navt.msg.intensity()
        start = end
        end += 1
        (val1.intensity,) = _struct_B.unpack(str[start:end])
        self.intensities.append(val1)
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill

_struct_I = genpy.struct_I
_struct_B = struct.Struct("<B")
