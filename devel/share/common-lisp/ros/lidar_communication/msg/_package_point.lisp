(cl:in-package lidar_communication-msg)
(cl:export '(INTENSITY-VAL
          INTENSITY
          ID-VAL
          ID
          DISTANCE-VAL
          DISTANCE
          VERTICAL_ANGLE-VAL
          VERTICAL_ANGLE
          X-VAL
          X
          Y-VAL
          Y
          Z-VAL
          Z
          U-VAL
          U
          V-VAL
          V
))