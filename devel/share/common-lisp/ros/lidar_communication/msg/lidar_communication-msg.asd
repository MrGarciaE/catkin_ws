
(cl:in-package :asdf)

(defsystem "lidar_communication-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "scan" :depends-on ("_package_scan"))
    (:file "_package_scan" :depends-on ("_package"))
    (:file "scan_360" :depends-on ("_package_scan_360"))
    (:file "_package_scan_360" :depends-on ("_package"))
    (:file "point" :depends-on ("_package_point"))
    (:file "_package_point" :depends-on ("_package"))
  ))