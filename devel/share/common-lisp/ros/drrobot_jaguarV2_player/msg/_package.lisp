(cl:defpackage drrobot_jaguarV2_player-msg
  (:use )
  (:export
   "<MOTORINFO>"
   "MOTORINFO"
   "<IMUINFO>"
   "IMUINFO"
   "<CUSTOMSENSOR>"
   "CUSTOMSENSOR"
   "<RANGE>"
   "RANGE"
   "<RANGEARRAY>"
   "RANGEARRAY"
   "<POWERINFO>"
   "POWERINFO"
   "<MOTORINFOARRAY>"
   "MOTORINFOARRAY"
   "<STANDARDSENSOR>"
   "STANDARDSENSOR"
  ))

