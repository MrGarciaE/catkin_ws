; Auto-generated. Do not edit!


(cl:in-package navt-msg)


;//! \htmlinclude branch.msg.html

(cl:defclass <branch> (roslisp-msg-protocol:ros-message)
  ((sub_region
    :reader sub_region
    :initarg :sub_region
    :type (cl:vector navt-msg:line)
   :initform (cl:make-array 0 :element-type 'navt-msg:line :initial-element (cl:make-instance 'navt-msg:line))))
)

(cl:defclass branch (<branch>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <branch>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'branch)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name navt-msg:<branch> is deprecated: use navt-msg:branch instead.")))

(cl:ensure-generic-function 'sub_region-val :lambda-list '(m))
(cl:defmethod sub_region-val ((m <branch>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:sub_region-val is deprecated.  Use navt-msg:sub_region instead.")
  (sub_region m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <branch>) ostream)
  "Serializes a message object of type '<branch>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'sub_region))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'sub_region))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <branch>) istream)
  "Deserializes a message object of type '<branch>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'sub_region) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'sub_region)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'navt-msg:line))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<branch>)))
  "Returns string type for a message object of type '<branch>"
  "navt/branch")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'branch)))
  "Returns string type for a message object of type 'branch"
  "navt/branch")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<branch>)))
  "Returns md5sum for a message object of type '<branch>"
  "515488fe4f406270b22eaa18cd13dc6c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'branch)))
  "Returns md5sum for a message object of type 'branch"
  "515488fe4f406270b22eaa18cd13dc6c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<branch>)))
  "Returns full string definition for message of type '<branch>"
  (cl:format cl:nil "line[] sub_region~%~%================================================================================~%MSG: navt/line~%statistics statistics~%identity identity~%point[] points~%~%================================================================================~%MSG: navt/statistics~%float64 sx~%float64 sy~%float64 sxx~%float64 syy~%float64 sxy~%float64 avr_x~%float64 avr_y~%float64 SSx~%float64 SSy~%float64 SSxy~%float64 lambda_1~%float64 lambda_2~%float64 theta1~%float64 theta2~%float64 rho1~%float64 rho2~%float64[2] eigVec1~%float64[2] eigVec2~%float64[2] eigVec3~%float64[2] eigVec4~%================================================================================~%MSG: navt/identity~%float64 distance~%float64 line_roughness~%uint8 qty_of_points~%uint8[] point_indexes~%float64[2] centroid~%uint16 azimuth_step~%float64[2] intensity~%uint16 planar_region~%uint16 numof_scan~%float64[2] laser_ids~%uint8[2] frontier_indexes~%~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'branch)))
  "Returns full string definition for message of type 'branch"
  (cl:format cl:nil "line[] sub_region~%~%================================================================================~%MSG: navt/line~%statistics statistics~%identity identity~%point[] points~%~%================================================================================~%MSG: navt/statistics~%float64 sx~%float64 sy~%float64 sxx~%float64 syy~%float64 sxy~%float64 avr_x~%float64 avr_y~%float64 SSx~%float64 SSy~%float64 SSxy~%float64 lambda_1~%float64 lambda_2~%float64 theta1~%float64 theta2~%float64 rho1~%float64 rho2~%float64[2] eigVec1~%float64[2] eigVec2~%float64[2] eigVec3~%float64[2] eigVec4~%================================================================================~%MSG: navt/identity~%float64 distance~%float64 line_roughness~%uint8 qty_of_points~%uint8[] point_indexes~%float64[2] centroid~%uint16 azimuth_step~%float64[2] intensity~%uint16 planar_region~%uint16 numof_scan~%float64[2] laser_ids~%uint8[2] frontier_indexes~%~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <branch>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'sub_region) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <branch>))
  "Converts a ROS message object to a list"
  (cl:list 'branch
    (cl:cons ':sub_region (sub_region msg))
))
