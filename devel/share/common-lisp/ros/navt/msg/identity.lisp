; Auto-generated. Do not edit!


(cl:in-package navt-msg)


;//! \htmlinclude identity.msg.html

(cl:defclass <identity> (roslisp-msg-protocol:ros-message)
  ((distance
    :reader distance
    :initarg :distance
    :type cl:float
    :initform 0.0)
   (line_roughness
    :reader line_roughness
    :initarg :line_roughness
    :type cl:float
    :initform 0.0)
   (qty_of_points
    :reader qty_of_points
    :initarg :qty_of_points
    :type cl:fixnum
    :initform 0)
   (point_indexes
    :reader point_indexes
    :initarg :point_indexes
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 0 :element-type 'cl:fixnum :initial-element 0))
   (centroid
    :reader centroid
    :initarg :centroid
    :type (cl:vector cl:float)
   :initform (cl:make-array 2 :element-type 'cl:float :initial-element 0.0))
   (azimuth_step
    :reader azimuth_step
    :initarg :azimuth_step
    :type cl:fixnum
    :initform 0)
   (intensity
    :reader intensity
    :initarg :intensity
    :type (cl:vector cl:float)
   :initform (cl:make-array 2 :element-type 'cl:float :initial-element 0.0))
   (planar_region
    :reader planar_region
    :initarg :planar_region
    :type cl:fixnum
    :initform 0)
   (numof_scan
    :reader numof_scan
    :initarg :numof_scan
    :type cl:fixnum
    :initform 0)
   (laser_ids
    :reader laser_ids
    :initarg :laser_ids
    :type (cl:vector cl:float)
   :initform (cl:make-array 2 :element-type 'cl:float :initial-element 0.0))
   (frontier_indexes
    :reader frontier_indexes
    :initarg :frontier_indexes
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 2 :element-type 'cl:fixnum :initial-element 0)))
)

(cl:defclass identity (<identity>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <identity>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'identity)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name navt-msg:<identity> is deprecated: use navt-msg:identity instead.")))

(cl:ensure-generic-function 'distance-val :lambda-list '(m))
(cl:defmethod distance-val ((m <identity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:distance-val is deprecated.  Use navt-msg:distance instead.")
  (distance m))

(cl:ensure-generic-function 'line_roughness-val :lambda-list '(m))
(cl:defmethod line_roughness-val ((m <identity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:line_roughness-val is deprecated.  Use navt-msg:line_roughness instead.")
  (line_roughness m))

(cl:ensure-generic-function 'qty_of_points-val :lambda-list '(m))
(cl:defmethod qty_of_points-val ((m <identity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:qty_of_points-val is deprecated.  Use navt-msg:qty_of_points instead.")
  (qty_of_points m))

(cl:ensure-generic-function 'point_indexes-val :lambda-list '(m))
(cl:defmethod point_indexes-val ((m <identity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:point_indexes-val is deprecated.  Use navt-msg:point_indexes instead.")
  (point_indexes m))

(cl:ensure-generic-function 'centroid-val :lambda-list '(m))
(cl:defmethod centroid-val ((m <identity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:centroid-val is deprecated.  Use navt-msg:centroid instead.")
  (centroid m))

(cl:ensure-generic-function 'azimuth_step-val :lambda-list '(m))
(cl:defmethod azimuth_step-val ((m <identity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:azimuth_step-val is deprecated.  Use navt-msg:azimuth_step instead.")
  (azimuth_step m))

(cl:ensure-generic-function 'intensity-val :lambda-list '(m))
(cl:defmethod intensity-val ((m <identity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:intensity-val is deprecated.  Use navt-msg:intensity instead.")
  (intensity m))

(cl:ensure-generic-function 'planar_region-val :lambda-list '(m))
(cl:defmethod planar_region-val ((m <identity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:planar_region-val is deprecated.  Use navt-msg:planar_region instead.")
  (planar_region m))

(cl:ensure-generic-function 'numof_scan-val :lambda-list '(m))
(cl:defmethod numof_scan-val ((m <identity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:numof_scan-val is deprecated.  Use navt-msg:numof_scan instead.")
  (numof_scan m))

(cl:ensure-generic-function 'laser_ids-val :lambda-list '(m))
(cl:defmethod laser_ids-val ((m <identity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:laser_ids-val is deprecated.  Use navt-msg:laser_ids instead.")
  (laser_ids m))

(cl:ensure-generic-function 'frontier_indexes-val :lambda-list '(m))
(cl:defmethod frontier_indexes-val ((m <identity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:frontier_indexes-val is deprecated.  Use navt-msg:frontier_indexes instead.")
  (frontier_indexes m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <identity>) ostream)
  "Serializes a message object of type '<identity>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'distance))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'line_roughness))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'qty_of_points)) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'point_indexes))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:write-byte (cl:ldb (cl:byte 8 0) ele) ostream))
   (cl:slot-value msg 'point_indexes))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'centroid))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'azimuth_step)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'azimuth_step)) ostream)
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'intensity))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'planar_region)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'planar_region)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'numof_scan)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'numof_scan)) ostream)
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'laser_ids))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:write-byte (cl:ldb (cl:byte 8 0) ele) ostream))
   (cl:slot-value msg 'frontier_indexes))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <identity>) istream)
  "Deserializes a message object of type '<identity>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'distance) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'line_roughness) (roslisp-utils:decode-double-float-bits bits)))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'qty_of_points)) (cl:read-byte istream))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'point_indexes) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'point_indexes)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:aref vals i)) (cl:read-byte istream)))))
  (cl:setf (cl:slot-value msg 'centroid) (cl:make-array 2))
  (cl:let ((vals (cl:slot-value msg 'centroid)))
    (cl:dotimes (i 2)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits)))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'azimuth_step)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'azimuth_step)) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'intensity) (cl:make-array 2))
  (cl:let ((vals (cl:slot-value msg 'intensity)))
    (cl:dotimes (i 2)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits)))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'planar_region)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'planar_region)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'numof_scan)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'numof_scan)) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'laser_ids) (cl:make-array 2))
  (cl:let ((vals (cl:slot-value msg 'laser_ids)))
    (cl:dotimes (i 2)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits)))))
  (cl:setf (cl:slot-value msg 'frontier_indexes) (cl:make-array 2))
  (cl:let ((vals (cl:slot-value msg 'frontier_indexes)))
    (cl:dotimes (i 2)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:aref vals i)) (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<identity>)))
  "Returns string type for a message object of type '<identity>"
  "navt/identity")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'identity)))
  "Returns string type for a message object of type 'identity"
  "navt/identity")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<identity>)))
  "Returns md5sum for a message object of type '<identity>"
  "86c810a0aa0cfa2a8ad2c2dd92047616")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'identity)))
  "Returns md5sum for a message object of type 'identity"
  "86c810a0aa0cfa2a8ad2c2dd92047616")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<identity>)))
  "Returns full string definition for message of type '<identity>"
  (cl:format cl:nil "float64 distance~%float64 line_roughness~%uint8 qty_of_points~%uint8[] point_indexes~%float64[2] centroid~%uint16 azimuth_step~%float64[2] intensity~%uint16 planar_region~%uint16 numof_scan~%float64[2] laser_ids~%uint8[2] frontier_indexes~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'identity)))
  "Returns full string definition for message of type 'identity"
  (cl:format cl:nil "float64 distance~%float64 line_roughness~%uint8 qty_of_points~%uint8[] point_indexes~%float64[2] centroid~%uint16 azimuth_step~%float64[2] intensity~%uint16 planar_region~%uint16 numof_scan~%float64[2] laser_ids~%uint8[2] frontier_indexes~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <identity>))
  (cl:+ 0
     8
     8
     1
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'point_indexes) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'centroid) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     2
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'intensity) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     2
     2
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'laser_ids) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'frontier_indexes) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <identity>))
  "Converts a ROS message object to a list"
  (cl:list 'identity
    (cl:cons ':distance (distance msg))
    (cl:cons ':line_roughness (line_roughness msg))
    (cl:cons ':qty_of_points (qty_of_points msg))
    (cl:cons ':point_indexes (point_indexes msg))
    (cl:cons ':centroid (centroid msg))
    (cl:cons ':azimuth_step (azimuth_step msg))
    (cl:cons ':intensity (intensity msg))
    (cl:cons ':planar_region (planar_region msg))
    (cl:cons ':numof_scan (numof_scan msg))
    (cl:cons ':laser_ids (laser_ids msg))
    (cl:cons ':frontier_indexes (frontier_indexes msg))
))
