(cl:in-package navt-msg)
(cl:export '(AVR_THETA1-VAL
          AVR_THETA1
          AVR_RHO1-VAL
          AVR_RHO1
          VECTOR_AB-VAL
          VECTOR_AB
          ANCHOR_POINT-VAL
          ANCHOR_POINT
          NORMAL-VAL
          NORMAL
          CENTROID-VAL
          CENTROID
          LIST_OF_PERIMETERS-VAL
          LIST_OF_PERIMETERS
          PERIMETER-VAL
          PERIMETER
          MAIN_BORDER-VAL
          MAIN_BORDER
))