; Auto-generated. Do not edit!


(cl:in-package navt-msg)


;//! \htmlinclude intensities.msg.html

(cl:defclass <intensities> (roslisp-msg-protocol:ros-message)
  ((intensities
    :reader intensities
    :initarg :intensities
    :type (cl:vector navt-msg:intensity)
   :initform (cl:make-array 0 :element-type 'navt-msg:intensity :initial-element (cl:make-instance 'navt-msg:intensity))))
)

(cl:defclass intensities (<intensities>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <intensities>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'intensities)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name navt-msg:<intensities> is deprecated: use navt-msg:intensities instead.")))

(cl:ensure-generic-function 'intensities-val :lambda-list '(m))
(cl:defmethod intensities-val ((m <intensities>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:intensities-val is deprecated.  Use navt-msg:intensities instead.")
  (intensities m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <intensities>) ostream)
  "Serializes a message object of type '<intensities>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'intensities))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'intensities))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <intensities>) istream)
  "Deserializes a message object of type '<intensities>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'intensities) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'intensities)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'navt-msg:intensity))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<intensities>)))
  "Returns string type for a message object of type '<intensities>"
  "navt/intensities")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'intensities)))
  "Returns string type for a message object of type 'intensities"
  "navt/intensities")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<intensities>)))
  "Returns md5sum for a message object of type '<intensities>"
  "86f723fcabace2bb83a99614e18dcfdc")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'intensities)))
  "Returns md5sum for a message object of type 'intensities"
  "86f723fcabace2bb83a99614e18dcfdc")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<intensities>)))
  "Returns full string definition for message of type '<intensities>"
  (cl:format cl:nil "intensity[] intensities~%================================================================================~%MSG: navt/intensity~%uint8 intensity~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'intensities)))
  "Returns full string definition for message of type 'intensities"
  (cl:format cl:nil "intensity[] intensities~%================================================================================~%MSG: navt/intensity~%uint8 intensity~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <intensities>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'intensities) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <intensities>))
  "Converts a ROS message object to a list"
  (cl:list 'intensities
    (cl:cons ':intensities (intensities msg))
))
