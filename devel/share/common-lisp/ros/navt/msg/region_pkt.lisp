; Auto-generated. Do not edit!


(cl:in-package navt-msg)


;//! \htmlinclude region_pkt.msg.html

(cl:defclass <region_pkt> (roslisp-msg-protocol:ros-message)
  ((regions
    :reader regions
    :initarg :regions
    :type (cl:vector navt-msg:region)
   :initform (cl:make-array 0 :element-type 'navt-msg:region :initial-element (cl:make-instance 'navt-msg:region))))
)

(cl:defclass region_pkt (<region_pkt>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <region_pkt>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'region_pkt)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name navt-msg:<region_pkt> is deprecated: use navt-msg:region_pkt instead.")))

(cl:ensure-generic-function 'regions-val :lambda-list '(m))
(cl:defmethod regions-val ((m <region_pkt>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:regions-val is deprecated.  Use navt-msg:regions instead.")
  (regions m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <region_pkt>) ostream)
  "Serializes a message object of type '<region_pkt>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'regions))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'regions))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <region_pkt>) istream)
  "Deserializes a message object of type '<region_pkt>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'regions) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'regions)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'navt-msg:region))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<region_pkt>)))
  "Returns string type for a message object of type '<region_pkt>"
  "navt/region_pkt")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'region_pkt)))
  "Returns string type for a message object of type 'region_pkt"
  "navt/region_pkt")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<region_pkt>)))
  "Returns md5sum for a message object of type '<region_pkt>"
  "567737777d097ca939dda37d9e46b41e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'region_pkt)))
  "Returns md5sum for a message object of type 'region_pkt"
  "567737777d097ca939dda37d9e46b41e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<region_pkt>)))
  "Returns full string definition for message of type '<region_pkt>"
  (cl:format cl:nil "region[] regions~%~%================================================================================~%MSG: navt/region~%parameters params~%line[] body~%branch[] branches~%bool grow~%bool branch~%~%================================================================================~%MSG: navt/parameters~%float64[2] avr_theta1~%float64[2] avr_rho1~%float64[3] vector_ab~%point anchor_point~%uint16 normal~%float64[3] centroid~%float64[] list_of_perimeters~%float64 perimeter~%chain[] main_border~%~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%================================================================================~%MSG: navt/chain~%point[] head_chain~%point[] tail_chain~%~%================================================================================~%MSG: navt/line~%statistics statistics~%identity identity~%point[] points~%~%================================================================================~%MSG: navt/statistics~%float64 sx~%float64 sy~%float64 sxx~%float64 syy~%float64 sxy~%float64 avr_x~%float64 avr_y~%float64 SSx~%float64 SSy~%float64 SSxy~%float64 lambda_1~%float64 lambda_2~%float64 theta1~%float64 theta2~%float64 rho1~%float64 rho2~%float64[2] eigVec1~%float64[2] eigVec2~%float64[2] eigVec3~%float64[2] eigVec4~%================================================================================~%MSG: navt/identity~%float64 distance~%float64 line_roughness~%uint8 qty_of_points~%uint8[] point_indexes~%float64[2] centroid~%uint16 azimuth_step~%float64[2] intensity~%uint16 planar_region~%uint16 numof_scan~%float64[2] laser_ids~%uint8[2] frontier_indexes~%~%================================================================================~%MSG: navt/branch~%line[] sub_region~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'region_pkt)))
  "Returns full string definition for message of type 'region_pkt"
  (cl:format cl:nil "region[] regions~%~%================================================================================~%MSG: navt/region~%parameters params~%line[] body~%branch[] branches~%bool grow~%bool branch~%~%================================================================================~%MSG: navt/parameters~%float64[2] avr_theta1~%float64[2] avr_rho1~%float64[3] vector_ab~%point anchor_point~%uint16 normal~%float64[3] centroid~%float64[] list_of_perimeters~%float64 perimeter~%chain[] main_border~%~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%================================================================================~%MSG: navt/chain~%point[] head_chain~%point[] tail_chain~%~%================================================================================~%MSG: navt/line~%statistics statistics~%identity identity~%point[] points~%~%================================================================================~%MSG: navt/statistics~%float64 sx~%float64 sy~%float64 sxx~%float64 syy~%float64 sxy~%float64 avr_x~%float64 avr_y~%float64 SSx~%float64 SSy~%float64 SSxy~%float64 lambda_1~%float64 lambda_2~%float64 theta1~%float64 theta2~%float64 rho1~%float64 rho2~%float64[2] eigVec1~%float64[2] eigVec2~%float64[2] eigVec3~%float64[2] eigVec4~%================================================================================~%MSG: navt/identity~%float64 distance~%float64 line_roughness~%uint8 qty_of_points~%uint8[] point_indexes~%float64[2] centroid~%uint16 azimuth_step~%float64[2] intensity~%uint16 planar_region~%uint16 numof_scan~%float64[2] laser_ids~%uint8[2] frontier_indexes~%~%================================================================================~%MSG: navt/branch~%line[] sub_region~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <region_pkt>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'regions) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <region_pkt>))
  "Converts a ROS message object to a list"
  (cl:list 'region_pkt
    (cl:cons ':regions (regions msg))
))
