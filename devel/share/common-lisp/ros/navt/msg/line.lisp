; Auto-generated. Do not edit!


(cl:in-package navt-msg)


;//! \htmlinclude line.msg.html

(cl:defclass <line> (roslisp-msg-protocol:ros-message)
  ((statistics
    :reader statistics
    :initarg :statistics
    :type navt-msg:statistics
    :initform (cl:make-instance 'navt-msg:statistics))
   (identity
    :reader identity
    :initarg :identity
    :type navt-msg:identity
    :initform (cl:make-instance 'navt-msg:identity))
   (points
    :reader points
    :initarg :points
    :type (cl:vector navt-msg:point)
   :initform (cl:make-array 0 :element-type 'navt-msg:point :initial-element (cl:make-instance 'navt-msg:point))))
)

(cl:defclass line (<line>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <line>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'line)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name navt-msg:<line> is deprecated: use navt-msg:line instead.")))

(cl:ensure-generic-function 'statistics-val :lambda-list '(m))
(cl:defmethod statistics-val ((m <line>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:statistics-val is deprecated.  Use navt-msg:statistics instead.")
  (statistics m))

(cl:ensure-generic-function 'identity-val :lambda-list '(m))
(cl:defmethod identity-val ((m <line>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:identity-val is deprecated.  Use navt-msg:identity instead.")
  (identity m))

(cl:ensure-generic-function 'points-val :lambda-list '(m))
(cl:defmethod points-val ((m <line>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:points-val is deprecated.  Use navt-msg:points instead.")
  (points m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <line>) ostream)
  "Serializes a message object of type '<line>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'statistics) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'identity) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'points))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'points))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <line>) istream)
  "Deserializes a message object of type '<line>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'statistics) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'identity) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'points) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'points)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'navt-msg:point))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<line>)))
  "Returns string type for a message object of type '<line>"
  "navt/line")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'line)))
  "Returns string type for a message object of type 'line"
  "navt/line")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<line>)))
  "Returns md5sum for a message object of type '<line>"
  "ecdba50a96a83a76fdf25a4363b867c7")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'line)))
  "Returns md5sum for a message object of type 'line"
  "ecdba50a96a83a76fdf25a4363b867c7")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<line>)))
  "Returns full string definition for message of type '<line>"
  (cl:format cl:nil "statistics statistics~%identity identity~%point[] points~%~%================================================================================~%MSG: navt/statistics~%float64 sx~%float64 sy~%float64 sxx~%float64 syy~%float64 sxy~%float64 avr_x~%float64 avr_y~%float64 SSx~%float64 SSy~%float64 SSxy~%float64 lambda_1~%float64 lambda_2~%float64 theta1~%float64 theta2~%float64 rho1~%float64 rho2~%float64[2] eigVec1~%float64[2] eigVec2~%float64[2] eigVec3~%float64[2] eigVec4~%================================================================================~%MSG: navt/identity~%float64 distance~%float64 line_roughness~%uint8 qty_of_points~%uint8[] point_indexes~%float64[2] centroid~%uint16 azimuth_step~%float64[2] intensity~%uint16 planar_region~%uint16 numof_scan~%float64[2] laser_ids~%uint8[2] frontier_indexes~%~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'line)))
  "Returns full string definition for message of type 'line"
  (cl:format cl:nil "statistics statistics~%identity identity~%point[] points~%~%================================================================================~%MSG: navt/statistics~%float64 sx~%float64 sy~%float64 sxx~%float64 syy~%float64 sxy~%float64 avr_x~%float64 avr_y~%float64 SSx~%float64 SSy~%float64 SSxy~%float64 lambda_1~%float64 lambda_2~%float64 theta1~%float64 theta2~%float64 rho1~%float64 rho2~%float64[2] eigVec1~%float64[2] eigVec2~%float64[2] eigVec3~%float64[2] eigVec4~%================================================================================~%MSG: navt/identity~%float64 distance~%float64 line_roughness~%uint8 qty_of_points~%uint8[] point_indexes~%float64[2] centroid~%uint16 azimuth_step~%float64[2] intensity~%uint16 planar_region~%uint16 numof_scan~%float64[2] laser_ids~%uint8[2] frontier_indexes~%~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <line>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'statistics))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'identity))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'points) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <line>))
  "Converts a ROS message object to a list"
  (cl:list 'line
    (cl:cons ':statistics (statistics msg))
    (cl:cons ':identity (identity msg))
    (cl:cons ':points (points msg))
))
