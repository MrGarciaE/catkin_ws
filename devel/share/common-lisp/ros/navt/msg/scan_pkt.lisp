; Auto-generated. Do not edit!


(cl:in-package navt-msg)


;//! \htmlinclude scan_pkt.msg.html

(cl:defclass <scan_pkt> (roslisp-msg-protocol:ros-message)
  ((scans
    :reader scans
    :initarg :scans
    :type (cl:vector navt-msg:scan)
   :initform (cl:make-array 0 :element-type 'navt-msg:scan :initial-element (cl:make-instance 'navt-msg:scan))))
)

(cl:defclass scan_pkt (<scan_pkt>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <scan_pkt>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'scan_pkt)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name navt-msg:<scan_pkt> is deprecated: use navt-msg:scan_pkt instead.")))

(cl:ensure-generic-function 'scans-val :lambda-list '(m))
(cl:defmethod scans-val ((m <scan_pkt>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:scans-val is deprecated.  Use navt-msg:scans instead.")
  (scans m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <scan_pkt>) ostream)
  "Serializes a message object of type '<scan_pkt>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'scans))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'scans))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <scan_pkt>) istream)
  "Deserializes a message object of type '<scan_pkt>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'scans) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'scans)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'navt-msg:scan))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<scan_pkt>)))
  "Returns string type for a message object of type '<scan_pkt>"
  "navt/scan_pkt")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'scan_pkt)))
  "Returns string type for a message object of type 'scan_pkt"
  "navt/scan_pkt")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<scan_pkt>)))
  "Returns md5sum for a message object of type '<scan_pkt>"
  "9971038ac37e60752a75f575bc240710")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'scan_pkt)))
  "Returns md5sum for a message object of type 'scan_pkt"
  "9971038ac37e60752a75f575bc240710")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<scan_pkt>)))
  "Returns full string definition for message of type '<scan_pkt>"
  (cl:format cl:nil "scan[] scans~%================================================================================~%MSG: navt/scan~%uint16 azimuth~%point[] points~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'scan_pkt)))
  "Returns full string definition for message of type 'scan_pkt"
  (cl:format cl:nil "scan[] scans~%================================================================================~%MSG: navt/scan~%uint16 azimuth~%point[] points~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <scan_pkt>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'scans) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <scan_pkt>))
  "Converts a ROS message object to a list"
  (cl:list 'scan_pkt
    (cl:cons ':scans (scans msg))
))
