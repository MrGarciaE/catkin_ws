; Auto-generated. Do not edit!


(cl:in-package navt-msg)


;//! \htmlinclude azm.msg.html

(cl:defclass <azm> (roslisp-msg-protocol:ros-message)
  ((line
    :reader line
    :initarg :line
    :type (cl:vector navt-msg:line)
   :initform (cl:make-array 0 :element-type 'navt-msg:line :initial-element (cl:make-instance 'navt-msg:line))))
)

(cl:defclass azm (<azm>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <azm>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'azm)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name navt-msg:<azm> is deprecated: use navt-msg:azm instead.")))

(cl:ensure-generic-function 'line-val :lambda-list '(m))
(cl:defmethod line-val ((m <azm>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:line-val is deprecated.  Use navt-msg:line instead.")
  (line m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <azm>) ostream)
  "Serializes a message object of type '<azm>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'line))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'line))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <azm>) istream)
  "Deserializes a message object of type '<azm>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'line) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'line)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'navt-msg:line))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<azm>)))
  "Returns string type for a message object of type '<azm>"
  "navt/azm")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'azm)))
  "Returns string type for a message object of type 'azm"
  "navt/azm")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<azm>)))
  "Returns md5sum for a message object of type '<azm>"
  "27047a6870985f12e65973274e3ea9c0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'azm)))
  "Returns md5sum for a message object of type 'azm"
  "27047a6870985f12e65973274e3ea9c0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<azm>)))
  "Returns full string definition for message of type '<azm>"
  (cl:format cl:nil "line[] line~%================================================================================~%MSG: navt/line~%statistics statistics~%identity identity~%point[] points~%~%================================================================================~%MSG: navt/statistics~%float64 sx~%float64 sy~%float64 sxx~%float64 syy~%float64 sxy~%float64 avr_x~%float64 avr_y~%float64 SSx~%float64 SSy~%float64 SSxy~%float64 lambda_1~%float64 lambda_2~%float64 theta1~%float64 theta2~%float64 rho1~%float64 rho2~%float64[2] eigVec1~%float64[2] eigVec2~%float64[2] eigVec3~%float64[2] eigVec4~%================================================================================~%MSG: navt/identity~%float64 distance~%float64 line_roughness~%uint8 qty_of_points~%uint8[] point_indexes~%float64[2] centroid~%uint16 azimuth_step~%float64[2] intensity~%uint16 planar_region~%uint16 numof_scan~%float64[2] laser_ids~%uint8[2] frontier_indexes~%~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'azm)))
  "Returns full string definition for message of type 'azm"
  (cl:format cl:nil "line[] line~%================================================================================~%MSG: navt/line~%statistics statistics~%identity identity~%point[] points~%~%================================================================================~%MSG: navt/statistics~%float64 sx~%float64 sy~%float64 sxx~%float64 syy~%float64 sxy~%float64 avr_x~%float64 avr_y~%float64 SSx~%float64 SSy~%float64 SSxy~%float64 lambda_1~%float64 lambda_2~%float64 theta1~%float64 theta2~%float64 rho1~%float64 rho2~%float64[2] eigVec1~%float64[2] eigVec2~%float64[2] eigVec3~%float64[2] eigVec4~%================================================================================~%MSG: navt/identity~%float64 distance~%float64 line_roughness~%uint8 qty_of_points~%uint8[] point_indexes~%float64[2] centroid~%uint16 azimuth_step~%float64[2] intensity~%uint16 planar_region~%uint16 numof_scan~%float64[2] laser_ids~%uint8[2] frontier_indexes~%~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <azm>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'line) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <azm>))
  "Converts a ROS message object to a list"
  (cl:list 'azm
    (cl:cons ':line (line msg))
))
