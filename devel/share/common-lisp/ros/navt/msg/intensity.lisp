; Auto-generated. Do not edit!


(cl:in-package navt-msg)


;//! \htmlinclude intensity.msg.html

(cl:defclass <intensity> (roslisp-msg-protocol:ros-message)
  ((intensity
    :reader intensity
    :initarg :intensity
    :type cl:fixnum
    :initform 0))
)

(cl:defclass intensity (<intensity>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <intensity>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'intensity)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name navt-msg:<intensity> is deprecated: use navt-msg:intensity instead.")))

(cl:ensure-generic-function 'intensity-val :lambda-list '(m))
(cl:defmethod intensity-val ((m <intensity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:intensity-val is deprecated.  Use navt-msg:intensity instead.")
  (intensity m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <intensity>) ostream)
  "Serializes a message object of type '<intensity>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'intensity)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <intensity>) istream)
  "Deserializes a message object of type '<intensity>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'intensity)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<intensity>)))
  "Returns string type for a message object of type '<intensity>"
  "navt/intensity")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'intensity)))
  "Returns string type for a message object of type 'intensity"
  "navt/intensity")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<intensity>)))
  "Returns md5sum for a message object of type '<intensity>"
  "cc4b43c8eec7a293badb5b2fa6f2cef0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'intensity)))
  "Returns md5sum for a message object of type 'intensity"
  "cc4b43c8eec7a293badb5b2fa6f2cef0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<intensity>)))
  "Returns full string definition for message of type '<intensity>"
  (cl:format cl:nil "uint8 intensity~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'intensity)))
  "Returns full string definition for message of type 'intensity"
  (cl:format cl:nil "uint8 intensity~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <intensity>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <intensity>))
  "Converts a ROS message object to a list"
  (cl:list 'intensity
    (cl:cons ':intensity (intensity msg))
))
