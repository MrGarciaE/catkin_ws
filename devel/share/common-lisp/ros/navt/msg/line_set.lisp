; Auto-generated. Do not edit!


(cl:in-package navt-msg)


;//! \htmlinclude line_set.msg.html

(cl:defclass <line_set> (roslisp-msg-protocol:ros-message)
  ((azm
    :reader azm
    :initarg :azm
    :type (cl:vector navt-msg:azm)
   :initform (cl:make-array 0 :element-type 'navt-msg:azm :initial-element (cl:make-instance 'navt-msg:azm))))
)

(cl:defclass line_set (<line_set>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <line_set>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'line_set)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name navt-msg:<line_set> is deprecated: use navt-msg:line_set instead.")))

(cl:ensure-generic-function 'azm-val :lambda-list '(m))
(cl:defmethod azm-val ((m <line_set>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:azm-val is deprecated.  Use navt-msg:azm instead.")
  (azm m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <line_set>) ostream)
  "Serializes a message object of type '<line_set>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'azm))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'azm))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <line_set>) istream)
  "Deserializes a message object of type '<line_set>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'azm) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'azm)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'navt-msg:azm))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<line_set>)))
  "Returns string type for a message object of type '<line_set>"
  "navt/line_set")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'line_set)))
  "Returns string type for a message object of type 'line_set"
  "navt/line_set")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<line_set>)))
  "Returns md5sum for a message object of type '<line_set>"
  "533dff7211a1851a107c566f6335af7c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'line_set)))
  "Returns md5sum for a message object of type 'line_set"
  "533dff7211a1851a107c566f6335af7c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<line_set>)))
  "Returns full string definition for message of type '<line_set>"
  (cl:format cl:nil "azm[] azm~%================================================================================~%MSG: navt/azm~%line[] line~%================================================================================~%MSG: navt/line~%statistics statistics~%identity identity~%point[] points~%~%================================================================================~%MSG: navt/statistics~%float64 sx~%float64 sy~%float64 sxx~%float64 syy~%float64 sxy~%float64 avr_x~%float64 avr_y~%float64 SSx~%float64 SSy~%float64 SSxy~%float64 lambda_1~%float64 lambda_2~%float64 theta1~%float64 theta2~%float64 rho1~%float64 rho2~%float64[2] eigVec1~%float64[2] eigVec2~%float64[2] eigVec3~%float64[2] eigVec4~%================================================================================~%MSG: navt/identity~%float64 distance~%float64 line_roughness~%uint8 qty_of_points~%uint8[] point_indexes~%float64[2] centroid~%uint16 azimuth_step~%float64[2] intensity~%uint16 planar_region~%uint16 numof_scan~%float64[2] laser_ids~%uint8[2] frontier_indexes~%~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'line_set)))
  "Returns full string definition for message of type 'line_set"
  (cl:format cl:nil "azm[] azm~%================================================================================~%MSG: navt/azm~%line[] line~%================================================================================~%MSG: navt/line~%statistics statistics~%identity identity~%point[] points~%~%================================================================================~%MSG: navt/statistics~%float64 sx~%float64 sy~%float64 sxx~%float64 syy~%float64 sxy~%float64 avr_x~%float64 avr_y~%float64 SSx~%float64 SSy~%float64 SSxy~%float64 lambda_1~%float64 lambda_2~%float64 theta1~%float64 theta2~%float64 rho1~%float64 rho2~%float64[2] eigVec1~%float64[2] eigVec2~%float64[2] eigVec3~%float64[2] eigVec4~%================================================================================~%MSG: navt/identity~%float64 distance~%float64 line_roughness~%uint8 qty_of_points~%uint8[] point_indexes~%float64[2] centroid~%uint16 azimuth_step~%float64[2] intensity~%uint16 planar_region~%uint16 numof_scan~%float64[2] laser_ids~%uint8[2] frontier_indexes~%~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <line_set>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'azm) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <line_set>))
  "Converts a ROS message object to a list"
  (cl:list 'line_set
    (cl:cons ':azm (azm msg))
))
