(cl:in-package navt-msg)
(cl:export '(DISTANCE-VAL
          DISTANCE
          LINE_ROUGHNESS-VAL
          LINE_ROUGHNESS
          QTY_OF_POINTS-VAL
          QTY_OF_POINTS
          POINT_INDEXES-VAL
          POINT_INDEXES
          CENTROID-VAL
          CENTROID
          AZIMUTH_STEP-VAL
          AZIMUTH_STEP
          INTENSITY-VAL
          INTENSITY
          PLANAR_REGION-VAL
          PLANAR_REGION
          NUMOF_SCAN-VAL
          NUMOF_SCAN
          LASER_IDS-VAL
          LASER_IDS
          FRONTIER_INDEXES-VAL
          FRONTIER_INDEXES
))