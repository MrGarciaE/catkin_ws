; Auto-generated. Do not edit!


(cl:in-package navt-msg)


;//! \htmlinclude region.msg.html

(cl:defclass <region> (roslisp-msg-protocol:ros-message)
  ((params
    :reader params
    :initarg :params
    :type navt-msg:parameters
    :initform (cl:make-instance 'navt-msg:parameters))
   (body
    :reader body
    :initarg :body
    :type (cl:vector navt-msg:line)
   :initform (cl:make-array 0 :element-type 'navt-msg:line :initial-element (cl:make-instance 'navt-msg:line)))
   (branches
    :reader branches
    :initarg :branches
    :type (cl:vector navt-msg:branch)
   :initform (cl:make-array 0 :element-type 'navt-msg:branch :initial-element (cl:make-instance 'navt-msg:branch)))
   (grow
    :reader grow
    :initarg :grow
    :type cl:boolean
    :initform cl:nil)
   (branch
    :reader branch
    :initarg :branch
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass region (<region>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <region>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'region)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name navt-msg:<region> is deprecated: use navt-msg:region instead.")))

(cl:ensure-generic-function 'params-val :lambda-list '(m))
(cl:defmethod params-val ((m <region>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:params-val is deprecated.  Use navt-msg:params instead.")
  (params m))

(cl:ensure-generic-function 'body-val :lambda-list '(m))
(cl:defmethod body-val ((m <region>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:body-val is deprecated.  Use navt-msg:body instead.")
  (body m))

(cl:ensure-generic-function 'branches-val :lambda-list '(m))
(cl:defmethod branches-val ((m <region>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:branches-val is deprecated.  Use navt-msg:branches instead.")
  (branches m))

(cl:ensure-generic-function 'grow-val :lambda-list '(m))
(cl:defmethod grow-val ((m <region>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:grow-val is deprecated.  Use navt-msg:grow instead.")
  (grow m))

(cl:ensure-generic-function 'branch-val :lambda-list '(m))
(cl:defmethod branch-val ((m <region>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:branch-val is deprecated.  Use navt-msg:branch instead.")
  (branch m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <region>) ostream)
  "Serializes a message object of type '<region>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'params) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'body))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'body))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'branches))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'branches))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'grow) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'branch) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <region>) istream)
  "Deserializes a message object of type '<region>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'params) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'body) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'body)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'navt-msg:line))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'branches) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'branches)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'navt-msg:branch))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
    (cl:setf (cl:slot-value msg 'grow) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'branch) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<region>)))
  "Returns string type for a message object of type '<region>"
  "navt/region")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'region)))
  "Returns string type for a message object of type 'region"
  "navt/region")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<region>)))
  "Returns md5sum for a message object of type '<region>"
  "af3ab4694c43984500d986e83b34d265")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'region)))
  "Returns md5sum for a message object of type 'region"
  "af3ab4694c43984500d986e83b34d265")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<region>)))
  "Returns full string definition for message of type '<region>"
  (cl:format cl:nil "parameters params~%line[] body~%branch[] branches~%bool grow~%bool branch~%~%================================================================================~%MSG: navt/parameters~%float64[2] avr_theta1~%float64[2] avr_rho1~%float64[3] vector_ab~%point anchor_point~%uint16 normal~%float64[3] centroid~%float64[] list_of_perimeters~%float64 perimeter~%chain[] main_border~%~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%================================================================================~%MSG: navt/chain~%point[] head_chain~%point[] tail_chain~%~%================================================================================~%MSG: navt/line~%statistics statistics~%identity identity~%point[] points~%~%================================================================================~%MSG: navt/statistics~%float64 sx~%float64 sy~%float64 sxx~%float64 syy~%float64 sxy~%float64 avr_x~%float64 avr_y~%float64 SSx~%float64 SSy~%float64 SSxy~%float64 lambda_1~%float64 lambda_2~%float64 theta1~%float64 theta2~%float64 rho1~%float64 rho2~%float64[2] eigVec1~%float64[2] eigVec2~%float64[2] eigVec3~%float64[2] eigVec4~%================================================================================~%MSG: navt/identity~%float64 distance~%float64 line_roughness~%uint8 qty_of_points~%uint8[] point_indexes~%float64[2] centroid~%uint16 azimuth_step~%float64[2] intensity~%uint16 planar_region~%uint16 numof_scan~%float64[2] laser_ids~%uint8[2] frontier_indexes~%~%================================================================================~%MSG: navt/branch~%line[] sub_region~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'region)))
  "Returns full string definition for message of type 'region"
  (cl:format cl:nil "parameters params~%line[] body~%branch[] branches~%bool grow~%bool branch~%~%================================================================================~%MSG: navt/parameters~%float64[2] avr_theta1~%float64[2] avr_rho1~%float64[3] vector_ab~%point anchor_point~%uint16 normal~%float64[3] centroid~%float64[] list_of_perimeters~%float64 perimeter~%chain[] main_border~%~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%================================================================================~%MSG: navt/chain~%point[] head_chain~%point[] tail_chain~%~%================================================================================~%MSG: navt/line~%statistics statistics~%identity identity~%point[] points~%~%================================================================================~%MSG: navt/statistics~%float64 sx~%float64 sy~%float64 sxx~%float64 syy~%float64 sxy~%float64 avr_x~%float64 avr_y~%float64 SSx~%float64 SSy~%float64 SSxy~%float64 lambda_1~%float64 lambda_2~%float64 theta1~%float64 theta2~%float64 rho1~%float64 rho2~%float64[2] eigVec1~%float64[2] eigVec2~%float64[2] eigVec3~%float64[2] eigVec4~%================================================================================~%MSG: navt/identity~%float64 distance~%float64 line_roughness~%uint8 qty_of_points~%uint8[] point_indexes~%float64[2] centroid~%uint16 azimuth_step~%float64[2] intensity~%uint16 planar_region~%uint16 numof_scan~%float64[2] laser_ids~%uint8[2] frontier_indexes~%~%================================================================================~%MSG: navt/branch~%line[] sub_region~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <region>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'params))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'body) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'branches) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <region>))
  "Converts a ROS message object to a list"
  (cl:list 'region
    (cl:cons ':params (params msg))
    (cl:cons ':body (body msg))
    (cl:cons ':branches (branches msg))
    (cl:cons ':grow (grow msg))
    (cl:cons ':branch (branch msg))
))
