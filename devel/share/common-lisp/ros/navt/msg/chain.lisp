; Auto-generated. Do not edit!


(cl:in-package navt-msg)


;//! \htmlinclude chain.msg.html

(cl:defclass <chain> (roslisp-msg-protocol:ros-message)
  ((head_chain
    :reader head_chain
    :initarg :head_chain
    :type (cl:vector navt-msg:point)
   :initform (cl:make-array 0 :element-type 'navt-msg:point :initial-element (cl:make-instance 'navt-msg:point)))
   (tail_chain
    :reader tail_chain
    :initarg :tail_chain
    :type (cl:vector navt-msg:point)
   :initform (cl:make-array 0 :element-type 'navt-msg:point :initial-element (cl:make-instance 'navt-msg:point))))
)

(cl:defclass chain (<chain>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <chain>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'chain)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name navt-msg:<chain> is deprecated: use navt-msg:chain instead.")))

(cl:ensure-generic-function 'head_chain-val :lambda-list '(m))
(cl:defmethod head_chain-val ((m <chain>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:head_chain-val is deprecated.  Use navt-msg:head_chain instead.")
  (head_chain m))

(cl:ensure-generic-function 'tail_chain-val :lambda-list '(m))
(cl:defmethod tail_chain-val ((m <chain>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:tail_chain-val is deprecated.  Use navt-msg:tail_chain instead.")
  (tail_chain m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <chain>) ostream)
  "Serializes a message object of type '<chain>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'head_chain))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'head_chain))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'tail_chain))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'tail_chain))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <chain>) istream)
  "Deserializes a message object of type '<chain>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'head_chain) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'head_chain)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'navt-msg:point))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'tail_chain) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'tail_chain)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'navt-msg:point))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<chain>)))
  "Returns string type for a message object of type '<chain>"
  "navt/chain")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'chain)))
  "Returns string type for a message object of type 'chain"
  "navt/chain")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<chain>)))
  "Returns md5sum for a message object of type '<chain>"
  "ab27ca08ee015fae10d6b44fbd106454")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'chain)))
  "Returns md5sum for a message object of type 'chain"
  "ab27ca08ee015fae10d6b44fbd106454")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<chain>)))
  "Returns full string definition for message of type '<chain>"
  (cl:format cl:nil "point[] head_chain~%point[] tail_chain~%~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'chain)))
  "Returns full string definition for message of type 'chain"
  (cl:format cl:nil "point[] head_chain~%point[] tail_chain~%~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <chain>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'head_chain) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'tail_chain) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <chain>))
  "Converts a ROS message object to a list"
  (cl:list 'chain
    (cl:cons ':head_chain (head_chain msg))
    (cl:cons ':tail_chain (tail_chain msg))
))
