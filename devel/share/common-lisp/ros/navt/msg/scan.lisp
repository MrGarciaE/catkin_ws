; Auto-generated. Do not edit!


(cl:in-package navt-msg)


;//! \htmlinclude scan.msg.html

(cl:defclass <scan> (roslisp-msg-protocol:ros-message)
  ((azimuth
    :reader azimuth
    :initarg :azimuth
    :type cl:fixnum
    :initform 0)
   (points
    :reader points
    :initarg :points
    :type (cl:vector navt-msg:point)
   :initform (cl:make-array 0 :element-type 'navt-msg:point :initial-element (cl:make-instance 'navt-msg:point))))
)

(cl:defclass scan (<scan>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <scan>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'scan)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name navt-msg:<scan> is deprecated: use navt-msg:scan instead.")))

(cl:ensure-generic-function 'azimuth-val :lambda-list '(m))
(cl:defmethod azimuth-val ((m <scan>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:azimuth-val is deprecated.  Use navt-msg:azimuth instead.")
  (azimuth m))

(cl:ensure-generic-function 'points-val :lambda-list '(m))
(cl:defmethod points-val ((m <scan>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader navt-msg:points-val is deprecated.  Use navt-msg:points instead.")
  (points m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <scan>) ostream)
  "Serializes a message object of type '<scan>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'azimuth)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'azimuth)) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'points))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'points))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <scan>) istream)
  "Deserializes a message object of type '<scan>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'azimuth)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'azimuth)) (cl:read-byte istream))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'points) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'points)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'navt-msg:point))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<scan>)))
  "Returns string type for a message object of type '<scan>"
  "navt/scan")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'scan)))
  "Returns string type for a message object of type 'scan"
  "navt/scan")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<scan>)))
  "Returns md5sum for a message object of type '<scan>"
  "62574e68bff04e888b13a2d8e5c6f6c1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'scan)))
  "Returns md5sum for a message object of type 'scan"
  "62574e68bff04e888b13a2d8e5c6f6c1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<scan>)))
  "Returns full string definition for message of type '<scan>"
  (cl:format cl:nil "uint16 azimuth~%point[] points~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'scan)))
  "Returns full string definition for message of type 'scan"
  (cl:format cl:nil "uint16 azimuth~%point[] points~%================================================================================~%MSG: navt/point~%uint8 intensity~%uint8 id~%uint16 distance~%float64 angle_d~%float64 angle_r~%float64 x~%float64 y~%float64 z~%float64 u~%float64 v~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <scan>))
  (cl:+ 0
     2
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'points) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <scan>))
  "Converts a ROS message object to a list"
  (cl:list 'scan
    (cl:cons ':azimuth (azimuth msg))
    (cl:cons ':points (points msg))
))
