(cl:in-package navt-msg)
(cl:export '(INTENSITY-VAL
          INTENSITY
          ID-VAL
          ID
          DISTANCE-VAL
          DISTANCE
          ANGLE_D-VAL
          ANGLE_D
          ANGLE_R-VAL
          ANGLE_R
          X-VAL
          X
          Y-VAL
          Y
          Z-VAL
          Z
          U-VAL
          U
          V-VAL
          V
))